package android.vb.com.vb.util;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.tozny.crypto.android.AesCbcWithIntegrity;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by SUJAN on 31-Aug-17.
 */

public class Constants extends Application {

    public static final String BASE_USER_URL = "http://healthpro.in/vb/restserver/index.php/welcome/";
    public static final String ACCESS_PREF = "232";
    public static final String ACCESS_PREF_KEY = "78946513123";
    public static final String USER_PREF = "963";
    public static final String USER_PREF_LOGIN_KEY = "45871";
    public static final String USER_PREF_SOCIAL_LOGIN_KEY = "45123871";
    public static final String USER_PREF_SKIP_KEY = "784962";
    public static final String USER_PREF_EMAIL_KEY = "1256";
    public static final String USER_PREF_MOBILE_KEY = "1279856";
    public static final String USER_PREF_NAME_KEY = "12114656";
    public static final String USER_PREF_LAST_NAME_KEY = "1219874156";
    public static final String USER_PREF_PASSWORD_KEY = "a745812";
    public static final String USER_PREF_USER_COUNTRY_KEY = "78523";
    public static final int PERMISSION_REQUEST_CODE = 1;
    public static final String CONTINUE_WATCHING_REFRESH = "CONTINUE_WATCHING_REFRESH";
    private static final String PASSWORD_PATTERN = "^.*(?=.{8,20})(?=.*\\d)(?=.*[a-zA-Z]).*$";
    public static String USER_COUNTRY;
    private static String USER_ID;
    private static AesCbcWithIntegrity.CipherTextIvMac cipherTextIvMac;
    private final Context context = this;

    // TODO: 23-Sep-17
    public static String getUserId() {
        return USER_ID;
    }

    // TODO: 23-Sep-17

    public static void setUserId(String userId) {

        USER_ID = userId;
    }

    public static boolean isEmailValid(String email) {

        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPasswordValid(final String password) {

        Matcher matcher = Pattern.compile(PASSWORD_PATTERN).matcher(password);
        return matcher.matches();

    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        return "";
    }


    // TODO: 23-Sep-17
    /*private static JealousSky initializeJealousSky(Context context) {
        JealousSky jealousSky = JealousSky.getInstance();
        try {
            String deviceId;
            deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            if (deviceId.equals("")) {
                deviceId = "FFD7BADF2FBB1999";
            }
            jealousSky.initialize("longestPasswordEverCreatedInAllTheUniverseOrMore", deviceId);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return jealousSky;
    }*/

    /*public static String encryptThis(Context context, String stringToEncrypt) {
        if (!stringToEncrypt.equals("")) {
            String encrypted = "";
            try {
                encrypted = initializeJealousSky(context).encryptToString(stringToEncrypt);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return encrypted;
        } else {
            return "";
        }
    }*/

   /* public static String decryptThis(Context context, String encryptedString) {
        if (!encryptedString.equals("")) {
            String decrypted = "";
            try {
                decrypted = initializeJealousSky(context).decryptToString(encryptedString);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return decrypted;
        } else {
            return "";
        }

    }*/

    public static String md5(String s) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes(Charset.forName("US-ASCII")), 0, s.length());
            byte[] magnitude = digest.digest();
            BigInteger bi = new BigInteger(1, magnitude);
            return String.format("%0" + (magnitude.length << 1) + "x", bi);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String bitmapToBase64(Bitmap bitmap) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encoded;

    }

    public static void fbKeyHash(Context context) {

        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "android.vb.com.vb",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }


    public static boolean checkPermission(Context context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;

        }
    }


    public static void requestPermission(Context activity) {

        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) activity, Manifest.permission.ACCESS_FINE_LOCATION)) {

            Toast.makeText(activity, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions((Activity) activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);


        } else {

            ActivityCompat.requestPermissions((Activity) activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

}

