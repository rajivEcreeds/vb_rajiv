package android.vb.com.vb.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import ca.mimic.oauth2library.OAuth2Client;
import ca.mimic.oauth2library.OAuthError;
import ca.mimic.oauth2library.OAuthResponse;

/**
 * Created by SUJAN on 02-Sep-17.
 */

public class AccessToken extends AsyncTask<Void, Void, String> {

    private Context mContext;

    public AccessToken(Context context) {
        this.mContext = context;
    }


    @Override
    protected String doInBackground(Void... voids) {

        try {

            OAuth2Client client = new OAuth2Client.Builder("f50d25af-2433-4734-b044-9582b68ce7d9", "-AB1mAj778sj_ai6k8BhKLoVAIat3cZyytocNtz8YKr4aqk595uTZJ-eCHXefMx9UqWT2WuXVsVCTtp8sxhLmw", "https://oauth.brightcove.com/v4/access_token").grantType("client_credentials").build();
            OAuthResponse response = client.requestAccessToken();
            if (response.isSuccessful()) {
                return response.getAccessToken();

            } else {
                OAuthError error = response.getOAuthError();
                return error.getError();
            }

        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    protected void onPostExecute(String t) {

        SharedPreferences.Editor tokenPreferenceEditor = mContext.getSharedPreferences(Constants.ACCESS_PREF, Context.MODE_PRIVATE).edit();
        tokenPreferenceEditor.putString(Constants.ACCESS_PREF_KEY, t);
        tokenPreferenceEditor.apply();
        tokenPreferenceEditor.commit();

    }

}


