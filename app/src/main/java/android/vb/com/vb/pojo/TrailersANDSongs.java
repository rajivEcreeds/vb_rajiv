package android.vb.com.vb.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by SUJAN on 15-Sep-17.
 */

public class TrailersANDSongs implements Parcelable {


    public static final Creator<TrailersANDSongs> CREATOR = new Creator<TrailersANDSongs>() {
        @Override
        public TrailersANDSongs createFromParcel(Parcel source) {
            return new TrailersANDSongs(source);
        }

        @Override
        public TrailersANDSongs[] newArray(int size) {
            return new TrailersANDSongs[size];
        }
    };
    /**
     * songid : 9
     * webseriesid : 3
     * episodetype : trailer
     * imagename :
     * bigimagename :
     * folderid : 5564385782001
     * tamilfolderid : 5565079330001
     * telugufolderid : 5564385782001
     * videoid : 5564385782001
     * tamilvideoid :
     * teluguvideoid :
     * createdate : 2017-10-13
     * status : 1
     * seriesid : 3
     * title : Twisted
     * description : Warning! Do not trust them!
     * longdescription : Amit Amritraaj, the owner of Amritraaj Constructions, has two years left to live. An empire of millions needs its next CEO. Amritraaj decides to play a game for the throne between his son, Arjun; his illegitimate son, Karan; and Veer, his employee who has been like a son to him<br>
     * generes : 16
     * cast : Nia Sharma, Namit Khanna, Tia Bajpai, Tanvi Vyas & Rahul Raj
     * epitype : paid
     * price : 150
     * pakinr : 160
     * banginr : 170
     * pound : 180
     * dollar : 190
     * image : Twisted POSTER horizontal final (2).jpg
     */

    private String songid;
    private String webseriesid;
    private String episodetype;
    private String imagename;
    private String bigimagename;
    private String folderid;
    private String tamilfolderid;
    private String telugufolderid;
    private String videoid;
    private String tamilvideoid;
    private String teluguvideoid;
    private String createdate;
    private String status;
    private String seriesid;
    private String title;
    private String description;
    private String longdescription;
    private String generes;
    private String cast;
    private String epitype;
    private String price;
    private String pakinr;
    private String banginr;
    private String pound;
    private String dollar;
    private String image;

    public TrailersANDSongs() {
    }

    protected TrailersANDSongs(Parcel in) {
        this.songid = in.readString();
        this.webseriesid = in.readString();
        this.episodetype = in.readString();
        this.imagename = in.readString();
        this.bigimagename = in.readString();
        this.folderid = in.readString();
        this.tamilfolderid = in.readString();
        this.telugufolderid = in.readString();
        this.videoid = in.readString();
        this.tamilvideoid = in.readString();
        this.teluguvideoid = in.readString();
        this.createdate = in.readString();
        this.status = in.readString();
        this.seriesid = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.longdescription = in.readString();
        this.generes = in.readString();
        this.cast = in.readString();
        this.epitype = in.readString();
        this.price = in.readString();
        this.pakinr = in.readString();
        this.banginr = in.readString();
        this.pound = in.readString();
        this.dollar = in.readString();
        this.image = in.readString();
    }

    public String getSongid() {
        return songid;
    }

    public void setSongid(String songid) {
        this.songid = songid;
    }

    public String getWebseriesid() {
        return webseriesid;
    }

    public void setWebseriesid(String webseriesid) {
        this.webseriesid = webseriesid;
    }

    public String getEpisodetype() {
        return episodetype;
    }

    public void setEpisodetype(String episodetype) {
        this.episodetype = episodetype;
    }

    public String getImagename() {
        return imagename;
    }

    public void setImagename(String imagename) {
        this.imagename = imagename;
    }

    public String getBigimagename() {
        return bigimagename;
    }

    public void setBigimagename(String bigimagename) {
        this.bigimagename = bigimagename;
    }

    public String getFolderid() {
        return folderid;
    }

    public void setFolderid(String folderid) {
        this.folderid = folderid;
    }

    public String getTamilfolderid() {
        return tamilfolderid;
    }

    public void setTamilfolderid(String tamilfolderid) {
        this.tamilfolderid = tamilfolderid;
    }

    public String getTelugufolderid() {
        return telugufolderid;
    }

    public void setTelugufolderid(String telugufolderid) {
        this.telugufolderid = telugufolderid;
    }

    public String getVideoid() {
        return videoid;
    }

    public void setVideoid(String videoid) {
        this.videoid = videoid;
    }

    public String getTamilvideoid() {
        return tamilvideoid;
    }

    public void setTamilvideoid(String tamilvideoid) {
        this.tamilvideoid = tamilvideoid;
    }

    public String getTeluguvideoid() {
        return teluguvideoid;
    }

    public void setTeluguvideoid(String teluguvideoid) {
        this.teluguvideoid = teluguvideoid;
    }

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSeriesid() {
        return seriesid;
    }

    public void setSeriesid(String seriesid) {
        this.seriesid = seriesid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLongdescription() {
        return longdescription;
    }

    public void setLongdescription(String longdescription) {
        this.longdescription = longdescription;
    }

    public String getGeneres() {
        return generes;
    }

    public void setGeneres(String generes) {
        this.generes = generes;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getEpitype() {
        return epitype;
    }

    public void setEpitype(String epitype) {
        this.epitype = epitype;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPakinr() {
        return pakinr;
    }

    public void setPakinr(String pakinr) {
        this.pakinr = pakinr;
    }

    public String getBanginr() {
        return banginr;
    }

    public void setBanginr(String banginr) {
        this.banginr = banginr;
    }

    public String getPound() {
        return pound;
    }

    public void setPound(String pound) {
        this.pound = pound;
    }

    public String getDollar() {
        return dollar;
    }

    public void setDollar(String dollar) {
        this.dollar = dollar;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "TrailersANDSongs{" +
                "songid='" + songid + '\'' +
                ", webseriesid='" + webseriesid + '\'' +
                ", episodetype='" + episodetype + '\'' +
                ", imagename='" + imagename + '\'' +
                ", bigimagename='" + bigimagename + '\'' +
                ", folderid='" + folderid + '\'' +
                ", tamilfolderid='" + tamilfolderid + '\'' +
                ", telugufolderid='" + telugufolderid + '\'' +
                ", videoid='" + videoid + '\'' +
                ", tamilvideoid='" + tamilvideoid + '\'' +
                ", teluguvideoid='" + teluguvideoid + '\'' +
                ", createdate='" + createdate + '\'' +
                ", status='" + status + '\'' +
                ", seriesid='" + seriesid + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", longdescription='" + longdescription + '\'' +
                ", generes='" + generes + '\'' +
                ", cast='" + cast + '\'' +
                ", epitype='" + epitype + '\'' +
                ", price='" + price + '\'' +
                ", pakinr='" + pakinr + '\'' +
                ", banginr='" + banginr + '\'' +
                ", pound='" + pound + '\'' +
                ", dollar='" + dollar + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.songid);
        dest.writeString(this.webseriesid);
        dest.writeString(this.episodetype);
        dest.writeString(this.imagename);
        dest.writeString(this.bigimagename);
        dest.writeString(this.folderid);
        dest.writeString(this.tamilfolderid);
        dest.writeString(this.telugufolderid);
        dest.writeString(this.videoid);
        dest.writeString(this.tamilvideoid);
        dest.writeString(this.teluguvideoid);
        dest.writeString(this.createdate);
        dest.writeString(this.status);
        dest.writeString(this.seriesid);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.longdescription);
        dest.writeString(this.generes);
        dest.writeString(this.cast);
        dest.writeString(this.epitype);
        dest.writeString(this.price);
        dest.writeString(this.pakinr);
        dest.writeString(this.banginr);
        dest.writeString(this.pound);
        dest.writeString(this.dollar);
        dest.writeString(this.image);
    }
}