package android.vb.com.vb.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.vb.com.vb.R;
import android.vb.com.vb.pojo.PurchaseHistory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by SUJAN on 14-Sep-17.
 */

public class PurchaseAdapter extends RecyclerView.Adapter<PurchaseAdapter.PurchaseViewHolder> {

    private LayoutInflater layoutInflater;
    private Context mContext;
    private List<PurchaseHistory> purchaseHistoryList = Collections.emptyList();


    public PurchaseAdapter(Context context, List<PurchaseHistory> purchase) {
        this.mContext = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.purchaseHistoryList = purchase;
    }


    @Override
    public PurchaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.adapter_purchase, parent, false);
        return new PurchaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PurchaseViewHolder holder, int position) {

        try {

            final PurchaseHistory finalPurchaseHistory = purchaseHistoryList.get(position);
            Picasso.with(mContext).load("http://healthpro.in/vb/public/webseries/" + finalPurchaseHistory.getSeriesid() + ".jpg").into(holder.seriesImageView);
            holder.seriesNameTextView.setText(finalPurchaseHistory.getTitle());
            holder.orderIdTextView.setText(finalPurchaseHistory.getOrderno());
            holder.priceTextView.setText(mContext.getResources().getString(R.string.rupee_symbol) + " " + finalPurchaseHistory.getAmount());
            holder.dateTextView.setText(finalPurchaseHistory.getOrderdate().substring(0, 10).replace('-', '/'));

        }catch (Exception e){

        }


    }

    @Override
    public int getItemCount() {
        return purchaseHistoryList.size();
    }

    class PurchaseViewHolder extends RecyclerView.ViewHolder {

        View view;
        CardView cardView;
        TextView seriesNameTextView, orderIdTextView, dateTextView, priceTextView, paymentModeTextView;
        ImageView seriesImageView;

        public PurchaseViewHolder(View itemView) {
            super(itemView);

            view = itemView;
            cardView = (CardView) view.findViewById(R.id.adapter_purchase_cardView);
            seriesNameTextView = (TextView) view.findViewById(R.id.adapter_purchase_series_name_textView);
            orderIdTextView = (TextView) view.findViewById(R.id.adapter_purchase_series_orderid_textView);
            dateTextView = (TextView) view.findViewById(R.id.adapter_purchase_series_date_textView);
            priceTextView = (TextView) view.findViewById(R.id.adapter_purchase_series_price_textView);
            paymentModeTextView = (TextView) view.findViewById(R.id.adapter_purchase_series_paymentMode_textView);
            seriesImageView = (ImageView) view.findViewById(R.id.adapter_purchase_series_imageView);


            cardView.setCardBackgroundColor(Color.parseColor("#000000"));
            cardView.setRadius(40.0f);


        }
    }
}



