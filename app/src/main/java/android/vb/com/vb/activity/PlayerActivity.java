package android.vb.com.vb.activity;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.vb.com.vb.R;
import android.vb.com.vb.database.DatabaseHandler;
import android.vb.com.vb.pojo.History;
import android.vb.com.vb.singleton.VolleySingleton;
import android.vb.com.vb.util.BrightcovePlayerActivity;
import android.vb.com.vb.util.Constants;
import android.vb.com.vb.util.Defaults;
import android.vb.com.vb.util.Quality;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.brightcove.player.edge.Catalog;
import com.brightcove.player.edge.VideoListener;
import com.brightcove.player.event.Event;
import com.brightcove.player.event.EventEmitter;
import com.brightcove.player.event.EventListener;
import com.brightcove.player.event.EventType;
import com.brightcove.player.media.DeliveryType;
import com.brightcove.player.mediacontroller.BrightcoveMediaController;
import com.brightcove.player.model.Source;
import com.brightcove.player.model.SourceCollection;
import com.brightcove.player.model.Video;
import com.brightcove.player.view.BaseVideoView;
import com.brightcove.player.view.BrightcoveExoPlayerVideoView;
import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;
import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBarWrapper;
import com.squareup.otto.Bus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.brightcove.player.mediacontroller.ShowHideController.HIDE_MEDIA_CONTROLS;
import static com.brightcove.player.mediacontroller.ShowHideController.SHOW_MEDIA_CONTROLS;


public class PlayerActivity extends BrightcovePlayerActivity {

    private static boolean isBuffering;
    //private String id, singleId;
    //  private Bundle detailedListBundle;
    FrameLayout container;
    //private EventEmitter eventEmitter;
    private Catalog catalog;
    private Toolbar toolbar;
    private AlphaAnimation fadeIn;
    private AlphaAnimation fadeOutOffset;
    private AlphaAnimation fadeOut;
    private ImageView playPause;
    private ProgressBar progressBar;
    private boolean isSingleVideo;
    private ArrayList<String> playlistArray = new ArrayList<>();
    private int startPoint = 0;
    private int singleStartPoint = 0;
    private String id, singleId;
    private Bundle detailedListBundle;
    private DatabaseHandler db;
    private Bus bus;
    private AudioManager audioManager = null;
    private Button rewindButton, forwardButton;
    private Map<Quality, Source> mp4Formats = new HashMap<Quality, Source>();
    private EventEmitter eventEmitter;
    private boolean isAuto = true;
    private Video video;
    private Source hlsSource = null;
    private Source mp4Source = null;
    private VideoState videoState = VideoState.VIDEO_NOT_STARTED;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bus = ((Defaults) getApplicationContext()).getBus();
        bus.register(this);
        db = new DatabaseHandler(PlayerActivity.this);
        setContentView(R.layout.activity_player);

        baseVideoView = (BrightcoveExoPlayerVideoView) findViewById(R.id.brightcove_video_view);
        initMediaController(baseVideoView);

        initView();
        prepareAnimation();
        initBrightcoveView();
        initBundle();

        catalog = new Catalog(eventEmitter, getString(R.string.account), getString(R.string.policy));


        //raj


        container = (FrameLayout) findViewById(R.id.container_player);
        eventEmitter = baseVideoView.getEventEmitter();

        eventEmitter.on(EventType.SELECT_SOURCE, new EventListener() {
            @Override
            public void processEvent(Event event) {
                video = (Video) event.properties.get(Event.VIDEO);
                Map<DeliveryType, SourceCollection> map = video.getSourceCollections();
                for (Map.Entry<DeliveryType, SourceCollection> entry : map.entrySet()) {
                    if (entry.getKey().equals(DeliveryType.MP4)) {
                        SourceCollection sc = entry.getValue();
                        Set<Source> set = sc.getSources();
                        for (Source src : set) {
                            switch (src.getIntegerProperty("height")) {
                                case 540:
                                    if (mp4Formats.get(Quality.HIGH) == null) {
                                        mp4Formats.put(Quality.HIGH, src);
                                    }
                                    break;
                                case 360:
                                    if (mp4Formats.get(Quality.MEDIUM) == null) {
                                        mp4Formats.put(Quality.MEDIUM, src);
                                    }
                                    break;
                                case 270:
                                    if (mp4Formats.get(Quality.LOW) == null) {
                                        mp4Formats.put(Quality.LOW, src);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    if (entry.getKey().equals(DeliveryType.HLS)) {
                        SourceCollection sc = entry.getValue();
                        Set<Source> set = sc.getSources();
                        for (Source src : set) {
                            String url = src.getUrl();
                            if (url.startsWith("https://")) {
                                hlsSource = src;
                                Log.d("SRC", hlsSource.toString());
                                break;
                            }
                        }
                    }
                }
                if (isAuto) {
                    if (hlsSource != null) {
                        event.preventDefault();
                        event.properties.put(Event.SOURCE, hlsSource);
                        eventEmitter.respond(event);
                        Log.d("SRC", "S: " + event.properties.get(Event.SOURCE));
                    }
                } else {
                    {
                        event.preventDefault();
                        event.properties.put(Event.SOURCE, mp4Formats.get(Quality.LOW));
                        Log.d("AAA", "processEvent: " + mp4Formats.get(Quality.LOW));
                        eventEmitter.respond(event);
                        Log.d("SRC", "A: " + event.properties.get(Event.SOURCE));
                    }
                }
            }
        });
        baseVideoView.getEventEmitter().emit(EventType.EXIT_FULL_SCREEN);
        final Catalog catalog = new Catalog(eventEmitter, getString(R.string.account), getString(R.string.policy));
        //  if (isSingleVideo) {


        ///ani

        if (isSingleVideo) {
            catalog.findVideoByID(singleId, new VideoListener() {
                @Override
                public void onVideo(Video video) {
                    getSupportActionBar().setTitle(String.valueOf(video.getProperties().get("name")));
                    baseVideoView.add(video);
                    baseVideoView.seekTo(singleStartPoint);
                    baseVideoView.start();
                    videoState = VideoState.VIDEO_STARTED;
                    onRewind(baseVideoView);
                    onForward(video, baseVideoView);
                }

                @Override
                public void onError(String s) {
                    throw new RuntimeException(s);
                }
            });
        } else {
            catalog.findVideoByID(playlistArray.get(startPoint), new VideoListener() {
                @Override
                public void onVideo(Video video) {
                    getSupportActionBar().setTitle(String.valueOf(video.getProperties().get("name")));
                    baseVideoView.add(video);
                    if (Constants.getUserId() != null) {
                        getTimestamp(video.getId());
                    } else if (db.getHistory(video.getId()) != null) {
                        baseVideoView.seekTo(Integer.parseInt(db.getHistory(video.getId()).getMillisecond()));
                        baseVideoView.start();
                        videoState = VideoState.VIDEO_STARTED;
                    } else {
                        baseVideoView.start();
                        videoState = VideoState.VIDEO_STARTED;
                    }

                    container.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            pauseButton();
                        }
                    });
                    onRewind(baseVideoView);
                    onForward(video, baseVideoView);


                }

                @Override
                public void onError(String s) {
                    throw new RuntimeException(s);
                }
            });
        }
    }

    private void initBrightcoveView() {
        eventEmitter = baseVideoView.getEventEmitter();
        baseVideoView.getEventEmitter().emit(HIDE_MEDIA_CONTROLS);
        baseVideoView.getEventEmitter().emit(SHOW_MEDIA_CONTROLS);
        baseVideoView.getEventEmitter().on(HIDE_MEDIA_CONTROLS, new EventListener() {
            @Override
            public void processEvent(Event event) {
                toolbar.startAnimation(fadeOut);
                if (!isBuffering) {
                    playPause.startAnimation(fadeOut);
                }
            }
        });

        baseVideoView.getEventEmitter().on(SHOW_MEDIA_CONTROLS, new EventListener() {
            @Override
            public void processEvent(Event event) {
                toolbar.startAnimation(fadeIn);
                toolbar.startAnimation(fadeOutOffset);
                if (!isBuffering) {
                    playPause.startAnimation(fadeIn);
                    playPause.startAnimation(fadeOutOffset);
                }
            }
        });

        baseVideoView.getEventEmitter().on(EventType.DID_PLAY, new EventListener() {
            @Override
            public void processEvent(Event event) {
                if (!isBuffering) {
                    playPause.setImageResource(R.drawable.ic_pause_white_24px);
                }
            }
        });

        baseVideoView.getEventEmitter().on(EventType.DID_PAUSE, new EventListener() {
            @Override
            public void processEvent(Event event) {
                if (!isBuffering) {
                    playPause.setImageResource(R.drawable.ic_play_arrow_white_24px);
                }
            }
        });

        baseVideoView.getEventEmitter().on(EventType.BUFFERING_STARTED, new EventListener() {
            @Override
            public void processEvent(Event event) {
                isBuffering = true;
                playPause.clearAnimation();
                playPause.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        baseVideoView.getEventEmitter().on(EventType.BUFFERING_COMPLETED, new EventListener() {
            @Override
            public void processEvent(Event event) {
                isBuffering = false;
                playPause.clearAnimation();
                playPause.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        });

        baseVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoState = VideoState.VIDEO_COMPLETED;
                removeVideo(baseVideoView.get(0).getId());
                if (isSingleVideo) {
                    finish();
                } else {
                    startPoint++;
                    catalog.findVideoByID(playlistArray.get(startPoint), new VideoListener() {
                        @Override
                        public void onVideo(Video video) {
                            getSupportActionBar().setTitle(String.valueOf(video.getProperties().get("name")));
                            baseVideoView.remove(0);
                            baseVideoView.add(video);
                            if (db.getHistory(video.getId()) != null) {
                                baseVideoView.seekTo(Integer.parseInt(db.getHistory(video.getId()).getMillisecond()));
                            }
                            baseVideoView.start();
                            videoState = VideoState.VIDEO_STARTED;
                        }
                    });
                }
            }
        });
    }

    private void removeVideo(String videoId) {

        String url2 = Constants.BASE_USER_URL + "updtatevideolengthdelete";

        Map<String, String> params = new HashMap<>();
        params.put("userid", Constants.getUserId());
        params.put("videoid", videoId);

        JsonObjectRequest removeVideoRequest = new JsonObjectRequest(Request.Method.POST, url2, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Aniket", "removeVideo > " + response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        VolleySingleton.getInstance(PlayerActivity.this).addToRequestQueue(removeVideoRequest);

    }

    private void initView() {
        baseVideoView = (BrightcoveExoPlayerVideoView) findViewById(R.id.brightcove_video_view);
        //playPause = (ImageView) findViewById(R.id.playPause);

      /*  progressBar = (ProgressBar) findViewById(R.id.progress_bar_1);
        toolbar = (Toolbar) findViewById(R.id.player_toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        setSupportActionBar(toolbar);*/


        playPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button playMediaControl = (Button) findViewById(R.id.play);
                playMediaControl.performClick();
            }
        });
    }

    private void sendTimestamp(final String videoId, final String timestamp) {

        String url = Constants.BASE_USER_URL + "updtatevideolength";

        Map<String, String> params = new HashMap<>();
        params.put("userid", Constants.getUserId());
        params.put("videoid", videoId);
        params.put("length", timestamp);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Aniket", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        VolleySingleton.getInstance(PlayerActivity.this).addToRequestQueue(request);
    }

    private void getTimestamp(String videoId) {

        String url = Constants.BASE_USER_URL + "videotimes";

        Map<String, String> params = new HashMap<>();
        params.put("userid", Constants.getUserId());
        params.put("videoid", videoId);


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.has("message") && response.getString("message") != "null") {
                        baseVideoView.seekTo(Integer.parseInt(response.getString("message")));
                        baseVideoView.start();
                        videoState = VideoState.VIDEO_STARTED;
                    } else {
                        baseVideoView.start();
                        videoState = VideoState.VIDEO_STARTED;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        VolleySingleton.getInstance(PlayerActivity.this).addToRequestQueue(request);

    }

    private void initBundle() {

        if (getIntent().hasExtra("video_id")) {
            id = getIntent().getExtras().getString("video_id");
        }

        detailedListBundle = new Bundle();

        if (getIntent().hasExtra("webseries")) {
            detailedListBundle.putParcelable("webseries", getIntent().getExtras().getParcelable("webseries"));
        }

        if (getIntent().hasExtra("banners")) {
            detailedListBundle.putParcelable("banners", getIntent().getExtras().getParcelable("banners"));
        }

        if (getIntent().hasExtra("user")) {
            detailedListBundle.putParcelable("user", getIntent().getExtras().getParcelable("user"));

        }

        if (getIntent().hasExtra("startPoint")) {
            startPoint = getIntent().getExtras().getInt("startPoint");
        }

        if (getIntent().hasExtra("playlistArray")) {
            playlistArray = getIntent().getStringArrayListExtra("playlistArray");
        }

        if (getIntent().hasExtra("isSingleVideo") && getIntent().getBooleanExtra("isSingleVideo", false)) {

            isSingleVideo = true;

            if (getIntent().hasExtra("videoId")) {
                singleId = getIntent().getExtras().getString("videoId");
            }

            if (getIntent().hasExtra("timestamp")) {
                singleStartPoint = Integer.parseInt(getIntent().getStringExtra("timestamp"));
            }

        }

    }

    private void prepareAnimation() {

        fadeIn = new AlphaAnimation(0f, 1f);
        fadeIn.setDuration(300);
        fadeIn.setFillAfter(true);

        fadeOut = new AlphaAnimation(1f, 0f);
        fadeOut.setDuration(300);
        fadeOut.setFillAfter(true);

        fadeOutOffset = new AlphaAnimation(1f, 0f);
        fadeOutOffset.setDuration(300);
        fadeOutOffset.setStartOffset(3000);
        fadeOutOffset.setFillAfter(true);

    }

    private void goBack() {

        if (isSingleVideo) {

            startActivity(new Intent(PlayerActivity.this, HomeActivity.class));

        } else if (detailedListBundle.getParcelableArray("user") != null) {

            if (detailedListBundle.getParcelable("webseries") != null) {
                startActivity(new Intent(PlayerActivity.this, DetailedListActivity.class).putExtra("user", detailedListBundle.getParcelable("user")).putExtra("webseries", detailedListBundle.getParcelable("webseries")));
            } else {
                startActivity(new Intent(PlayerActivity.this, DetailedListActivity.class).putExtra("user", detailedListBundle.getParcelable("user")).putExtra("banners", detailedListBundle.getParcelable("banners")));
            }
            finish();
        } else {

            if (detailedListBundle.getParcelable("webseries") != null) {
                startActivity(new Intent(PlayerActivity.this, DetailedListActivity.class).putExtra("webseries", detailedListBundle.getParcelable("webseries")));
            } else {
                startActivity(new Intent(PlayerActivity.this, DetailedListActivity.class).putExtra("banners", detailedListBundle.getParcelable("banners")));
            }
            finish();
        }
    }

    /* qq
    private void goBack() {

        if (detailedListBundle.getParcelableArray("user") != null) {
            if (detailedListBundle.getParcelable("webseries") != null) {
                startActivity(new Intent(PlayerActivity.this, DetailedListActivity.class).putExtra("user", detailedListBundle.getParcelable("user")).putExtra("webseries", detailedListBundle.getParcelable("webseries")));
            } else {
                startActivity(new Intent(PlayerActivity.this, DetailedListActivity.class).putExtra("user", detailedListBundle.getParcelable("user")).putExtra("banners", detailedListBundle.getParcelable("banners")));
            }
            finish();
        } else {
            if (detailedListBundle.getParcelable("webseries") != null) {
                startActivity(new Intent(PlayerActivity.this, DetailedListActivity.class).putExtra("webseries", detailedListBundle.getParcelable("webseries")));
            } else {
                startActivity(new Intent(PlayerActivity.this, DetailedListActivity.class).putExtra("banners", detailedListBundle.getParcelable("banners")));
            }
            finish();
        }
    }*/

    @Override
    protected void onDestroy() {
        if (baseVideoView != null && videoState == VideoState.VIDEO_STARTED) {
            if (Constants.getUserId() != null) {
                sendTimestamp(baseVideoView.getCurrentVideo().getId(), String.valueOf(baseVideoView.getCurrentPosition()));
            } else {
                db.addHistory(
                        new History(
                                baseVideoView.getCurrentVideo().getId(),
                                String.valueOf(baseVideoView.getCurrentPosition()),
                                String.valueOf(baseVideoView.get(0).getProperties().get("name")),
                                String.valueOf(baseVideoView.get(0).getProperties().get("thumbnail"))
                        )
                );
            }
        } else if (videoState == VideoState.VIDEO_COMPLETED) {
            bus.post(Constants.CONTINUE_WATCHING_REFRESH);
        }
        super.onDestroy();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void onRewind(final BaseVideoView baseVideoView) {

        rewindButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (baseVideoView.getCurrentPosition() > 10000) {

                    baseVideoView.seekTo(baseVideoView.getCurrentPosition() - 10000);

                } else {
                    baseVideoView.seekTo(0);
                }

            }
        });


    }

    public void onForward(final Video video, final BaseVideoView baseVideoView) {

        forwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (baseVideoView.getCurrentPosition() >= video.getDuration() - 10000) {
                    baseVideoView.seekTo(video.getDuration());
                } else {
                    baseVideoView.seekTo(baseVideoView.getCurrentPosition() + 10000);
                    Log.d("DAI", baseVideoView.getVideoHeight() + " X " + baseVideoView.getVideoWidth());
                }
            }
        });
    }

    private void pauseButton() {
        Button play = (Button) findViewById(R.id.play);
        play.performClick();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            goBack();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initToolbar(final BaseVideoView brightcoveVideoView) {
        toolbar = (Toolbar) brightcoveVideoView.findViewById(R.id.activity_player_toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setTitle(playlistArray.get(startPoint));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

    }

    private void initButtons(final BaseVideoView brightcoveVideoView) {

        Button volumeButton = (Button) brightcoveVideoView.findViewById(R.id.volume);
        playPause = (ImageView) brightcoveVideoView.findViewById(R.id.playPause);
        final VerticalSeekBarWrapper verticalSeekBarWrapper = (VerticalSeekBarWrapper) brightcoveVideoView.findViewById(R.id.seekVolumeControl);
        volumeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (verticalSeekBarWrapper.getVisibility() == View.GONE) {
                    verticalSeekBarWrapper.setVisibility(View.VISIBLE);

                    try {
                        VerticalSeekBar volumeSeekbar = (VerticalSeekBar) findViewById(R.id.volumeSeekBar);
                        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        volumeSeekbar.setMax(audioManager
                                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
                        volumeSeekbar.setProgress(audioManager
                                .getStreamVolume(AudioManager.STREAM_MUSIC));


                        volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                            @Override
                            public void onStopTrackingTouch(SeekBar arg0) {
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar arg0) {
                            }

                            @Override
                            public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
                                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                                        progress, 0);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    verticalSeekBarWrapper.setVisibility(View.GONE);
                }
            }
        });

        //initCustomSpinner(brightcoveVideoView);
        rewindButton = (Button) brightcoveVideoView.findViewById(R.id.rewind);
        forwardButton = (Button) brightcoveVideoView.findViewById(R.id.fast_forward);

        initToolbar(brightcoveVideoView);
    }

    public void initMediaController(final BaseVideoView brightcoveVideoView) {
        brightcoveVideoView.setMediaController(new BrightcoveMediaController(brightcoveVideoView, R.layout.player_media_control));
        initButtons(brightcoveVideoView);

        // This event is sent by the BrightcovePlayer Activity when the onConfigurationChanged has been called.
        brightcoveVideoView.getEventEmitter().on(EventType.CONFIGURATION_CHANGED, new EventListener() {
            @Override
            public void processEvent(Event event) {
                initButtons(brightcoveVideoView);
            }
        });
    }

    private enum VideoState {VIDEO_NOT_STARTED, VIDEO_STARTED, VIDEO_COMPLETED}

}
