package android.vb.com.vb.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.vb.com.vb.R;
import android.vb.com.vb.adapter.VerticalEpisodeAdapter;
import android.vb.com.vb.airpay.MainActivity;
import android.vb.com.vb.database.DatabaseHandler;
import android.vb.com.vb.pojo.Banner;
import android.vb.com.vb.pojo.User;
import android.vb.com.vb.pojo.Video;
import android.vb.com.vb.pojo.Webseries;
import android.vb.com.vb.singleton.VolleySingleton;
import android.vb.com.vb.util.AccessToken;
import android.vb.com.vb.util.Constants;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailedListActivity extends AppCompatActivity {


    private static String selectedLang = "hindi";
    final List<String> lang = new ArrayList<>();
    ProgressBar progressBar;
    int selectedIndex = 0;
    private RecyclerView episodeRecyclerView;
    private Toolbar toolbar;
    private ImageView playImageView, showcasePosterImageView, paytoBingImageView, playTrailerImageView;
    private TextView bingPriceTextView;
    private Webseries webseries;
    private Banner banner;
    private User user;
    private Video[] videos1;
    private List<Video> videos = new ArrayList<>();
    private Context mContext = this;
    private String showcaseVideoId;
    private TextView totalVideoTextView, showcaseEpisodesNumberTextView, seriesLongDescriptionTextView, moreDetailsToggleTextView, moreDetailsContentTextView, seriesRating, playToWatchTailerTextView, castTextView;
    private RatingBar ratingBar;
    private FrameLayout paidDimmerFrameLayout;
    private Bundle bundle;
    private ProgressDialog progressDialog;
    private boolean isBanner;
    private boolean isBinge;
    private boolean isPaid;
    private ArrayList<String> playlistVideo = new ArrayList<>();
    private TextView languageSelector;
    private DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_list);
        databaseHandler = new DatabaseHandler(DetailedListActivity.this);

        init();
        //qq  progressBar.setVisibility(View.VISIBLE);

        progressDialog.show();
        //progressView.startAnimation();

        setupToolbarMenu();
        getIntentData();
        getSelectedLanguage();
        getVideosData();
        onMoreToggleClicked();
        onPayToBingButtonClicked();
        onLanguageChange();


    }

    private void setTrailerClick() {

        String trailerId = "";

        if (databaseHandler.getLanguage(webseries.getSeriesid()) != null) {
            selectedLang = databaseHandler.getLanguage(webseries.getSeriesid());
            Toast.makeText(mContext, selectedLang, Toast.LENGTH_SHORT).show();
            switch (selectedLang) {

                case "tamil":
                    // Toast.makeText(mContext, "This is tamil : "+ webseries.getTamiltrailer(), Toast.LENGTH_SHORT).show();
                    trailerId = webseries.getTamiltrailer();
                    startActivity(new Intent(this, PlayerActivity.class)
                            .putExtra("isSingleVideo", true)
                            .putExtra("videoId", trailerId));
                    break;
                case "telugu":
                    // Toast.makeText(mContext, "This is telego: " + webseries.getTelugutrailer(), Toast.LENGTH_SHORT).show();

                    trailerId = webseries.getTelugutrailer();
                    startActivity(new Intent(this, PlayerActivity.class)
                            .putExtra("isSingleVideo", true)
                            .putExtra("videoId", trailerId));
                    break;
                default:
                    // Toast.makeText(mContext, "This is hindi: " +webseries.getHinditrailer(), Toast.LENGTH_SHORT).show();

                    trailerId = webseries.getHinditrailer();
                    startActivity(new Intent(this, PlayerActivity.class)
                            .putExtra("isSingleVideo", true)
                            .putExtra("videoId", trailerId));
                    break;
            }

        } else {

            // Toast.makeText(mContext, "This israndom", Toast.LENGTH_SHORT).show();

            startActivity(new Intent(this, PlayerActivity.class)
                    .putExtra("isSingleVideo", true)
                    .putExtra("videoId", trailerId));

        }



        /*if (webseries != null) {
            if (databaseHandler.getLanguage(webseries.getSeriesid()) != null) {
                selectedLang = databaseHandler.getLanguage(webseries.getSeriesid());
                Toast.makeText(mContext, selectedLang, Toast.LENGTH_SHORT).show();
                switch (selectedLang) {

                    case "tamil":
                        trailerId = webseries.getTamiltrailer();
                        startActivity(new Intent(this, PlayerActivity.class)
                                .putExtra("isSingleVideo", true)
                                .putExtra("videoId", trailerId));
                        break;
                    case "telugu":
                        trailerId = webseries.getTelugutrailer();
                        startActivity(new Intent(this, PlayerActivity.class)
                                .putExtra("isSingleVideo", true)
                                .putExtra("videoId", trailerId));
                        break;
                    default:
                        trailerId = webseries.getHinditrailer();
                        startActivity(new Intent(this, PlayerActivity.class)
                                .putExtra("isSingleVideo", true)
                                .putExtra("videoId", trailerId));
                        break;
                }

                startActivity(new Intent(this, PlayerActivity.class)
                        .putExtra("isSingleVideo", true)
                        .putExtra("videoId", ""));

            } else {
                trailerId = webseries.getHinditrailer();
                startActivity(new Intent(this, PlayerActivity.class)
                        .putExtra("isSingleVideo", true)
                        .putExtra("videoId", ""));
            }

        }*/

        /*else {


            if (databaseHandler.getLanguage(banner.getSeriesid()) != null) {
                selectedLang = databaseHandler.getLanguage(banner.getSeriesid());
                Toast.makeText(mContext, selectedLang, Toast.LENGTH_SHORT).show();
            *//*for (int i = 0; i < lang.size(); i++) {
                if (selectedLang.equals(lang.get(i).toLowerCase())) {
                    selectedIndex = i;
                    languageSelector.setText(lang.get(i).toUpperCase());
                }
            }*//*

                switch (selectedLang) {

                    case "tamil":
                        trailerId = banner.getTamiltrailer();
                        break;
                    case "telugu":
                        trailerId = banner.getTelugutrailer();
                        break;
                    default:
                        trailerId = banner.getHinditrailer();
                        break;
                }

            } else {
                trailerId = webseries.getHinditrailer();
            }

            }*/


    }


    private void getSelectedLanguage() {

        if (webseries != null) {

            if (databaseHandler.getLanguage(webseries.getSeriesid()) != null) {
                selectedLang = databaseHandler.getLanguage(webseries.getSeriesid());
                Toast.makeText(mContext, selectedLang, Toast.LENGTH_SHORT).show();
                for (int i = 0; i < lang.size(); i++) {
                    if (selectedLang.equals(lang.get(i).toLowerCase())) {
                        selectedIndex = i;
                        languageSelector.setText(lang.get(i).toUpperCase());
                    }
                }
            }
        }
    }

    private void onLanguageChange() {

        if (lang.size() > 1) {

            languageSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    for (int i = 0; i < lang.size(); i++) {
                        if (selectedLang.equals(lang.get(i).toLowerCase())) {
                            selectedIndex = i;
                        }
                    }

                    new AlertDialog.Builder(DetailedListActivity.this, R.style.Theme_AppCompat_Dialog)
                            .setTitle("Select language")
                            .setSingleChoiceItems(lang.toArray(new CharSequence[lang.size()]), selectedIndex, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    if (!selectedLang.equals(lang.get(i).toLowerCase())) {
                                        selectedLang = lang.get(i).toLowerCase();
                                        databaseHandler.setLanguage(webseries.getSeriesid(), selectedLang);
                                        languageSelector.setText(lang.get(i).toUpperCase());
                                        getShowcaseVideo();
                                    }

                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();
                }
            });
        }

    }

    private void init() {

        playImageView = (ImageView) findViewById(R.id.activity_detailed_list_play_imageView);
        showcasePosterImageView = (ImageView) findViewById(R.id.activity_detailed_list_toolbarImage);
        languageSelector = (TextView) findViewById(R.id.activity_detailed_lang_selector);
        totalVideoTextView = (TextView) findViewById(R.id.activity_detailed_list_total_videos_textView);
        showcaseEpisodesNumberTextView = (TextView) findViewById(R.id.activity_detailed_list_showcase_totalEpisodes_textView);
        seriesLongDescriptionTextView = (TextView) findViewById(R.id.activity_detailed_list_longDescription_TextView);
        moreDetailsToggleTextView = (TextView) findViewById(R.id.activity_detailed_list_more_details_toggle_TextView);
        moreDetailsContentTextView = (TextView) findViewById(R.id.activity_detailed_list_more_details_content_TextView);
        ratingBar = (RatingBar) findViewById(R.id.activity_detailed_list_ratingBar);
        seriesRating = (TextView) findViewById(R.id.activity_detailed_list_series_rating_textView);
        paytoBingImageView = (ImageView) findViewById(R.id.activity_detailed_list_play_for_bing_ImageView);
        paidDimmerFrameLayout = (FrameLayout) findViewById(R.id.activity_detailed_list_paid_dimmer_framelayout);
        playToWatchTailerTextView = (TextView) findViewById(R.id.activity_detailed_list_play_for_bing_trailer_textView);
        playTrailerImageView = (ImageView) findViewById(R.id.activity_detailed_list_play_trailer_imageView);
        castTextView = (TextView) findViewById(R.id.activity_detailed_list_cast_TextView);
        bingPriceTextView = (TextView) findViewById(R.id.activity_detailed_list_play_for_bing_price_textView);
        //qq progressBar = (ProgressBar) findViewById(R.id.act_detailed_progress_bar);


        progressDialog = new ProgressDialog(this, R.style.MyTheme);
        //progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        progressDialog.setCancelable(false);

        //progressView = (CircularProgressView) findViewById(R.id.activity_detailed_list_progress_view);


        setRating();
//

        episodeRecyclerView = (RecyclerView) findViewById(R.id.activity_detailed_list_recyclerView);
    }

    private void onMoreToggleClicked() {

        moreDetailsToggleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (moreDetailsContentTextView.isShown()) {

                    seriesLongDescriptionTextView.setMaxLines(1);
                    moreDetailsContentTextView.setVisibility(View.GONE);

                } else {
                    seriesLongDescriptionTextView.setMaxLines(Integer.MAX_VALUE);
                    moreDetailsContentTextView.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    private void onPayToBingButtonClicked() {

        paytoBingImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getSharedPreferences(Constants.USER_PREF, MODE_PRIVATE).getBoolean(Constants.USER_PREF_SKIP_KEY, true)) {

                    startActivity(new Intent(DetailedListActivity.this, LoginActivity.class));

                } else {

                    if (webseries != null) {
                        startActivity(new Intent(DetailedListActivity.this, MainActivity.class).putExtra("webseries", bundle.getParcelable("webseries")).putExtra("user", bundle.getParcelable("user")));
                    } else {
                        startActivity(new Intent(DetailedListActivity.this, MainActivity.class).putExtra("banners", bundle.getParcelable("banners")).putExtra("user", bundle.getParcelable("user")));
                    }
                }
            }
        });
    }

    private void initRating(final String seriesId) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "seriesrating",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (!response.contains("Couldn't find any Ratings!") && !response.contains("false")) {
                            seriesRating.setVisibility(View.VISIBLE);
                            seriesRating.setText(String.valueOf((Float.valueOf(response))).substring(0, 3));

                        } else {
                            seriesRating.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("webseriesid", seriesId);
                return params;
            }
        };
        VolleySingleton.getInstance(DetailedListActivity.this).addToRequestQueue(stringRequest);
    }

    private void onPlayButtonClicked() {
        playImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playVideo();
            }
        });
    }

    private void onPlayTrailerButtonClicked() {
        playTrailerImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //asdplayVideo();
                setTrailerClick();
            }
        });
    }


    /*      if (databaseHandler.getLanguage(webseries.getSeriesid()) != null) {
            selectedLang = databaseHandler.getLanguage(webseries.getSeriesid());
            Toast.makeText(mContext, selectedLang, Toast.LENGTH_SHORT).show();
            /*for (int i = 0; i < lang.size(); i++) {
                if (selectedLang.equals(lang.get(i).toLowerCase())) {
                    selectedIndex = i;
                    languageSelector.setText(lang.get(i).toUpperCase());
                }
            }

            switch (selectedLang){

        case "tamil":
            //
            break;
        case "telugu":
            //
            break;
        default:
            //
            break;
    }

}
    */
    private void playVideo() {


        if (getSharedPreferences(Constants.USER_PREF, MODE_PRIVATE).getBoolean(Constants.USER_PREF_SKIP_KEY, true)) {

            Intent i = new Intent(DetailedListActivity.this, PlayerActivity.class);
            i.putExtra("video_id", (showcaseVideoId != null) ? showcaseVideoId : "");

            if (webseries != null) {
                i.putExtra("webseries", bundle.getParcelable("webseries"));
                i.putExtra("startPoint", 0);
                i.putStringArrayListExtra("playlistArray", playlistVideo);
                startActivity(i);
            } else {
                i.putExtra("banners", bundle.getParcelable("banners"));
                i.putExtra("startPoint", 0);
                i.putStringArrayListExtra("playlistArray", playlistVideo);
                startActivity(i);
            }

        } else {
            Intent i = new Intent(DetailedListActivity.this, PlayerActivity.class);
            i.putExtra("video_id", (showcaseVideoId != null) ? showcaseVideoId : "");

            if (webseries != null) {
                i.putExtra("webseries", bundle.getParcelable("webseries"));
                i.putExtra("user", bundle.getParcelable("user"));
                i.putExtra("startPoint", 0);
                i.putStringArrayListExtra("playlistArray", playlistVideo);
                startActivity(i);
            } else {
                i.putExtra("banners", bundle.getParcelable("banners"));
                i.putExtra("user", bundle.getParcelable("user"));
                i.putExtra("startPoint", 0);
                i.putStringArrayListExtra("playlistArray", playlistVideo);
                startActivity(i);
            }
        }
    }

    private void getIntentData() {

        bundle = new Bundle();

        if (getIntent().hasExtra("banners")) {

            banner = getIntent().getExtras().getParcelable("banners");
            isBanner = true;
            if (banner != null) {
                ((TextView) toolbar.findViewById(R.id.toolbar_text_title)).setText(banner.getTitle().trim().toUpperCase());
                ((TextView) toolbar.findViewById(R.id.toolbar_text_subtitle)).setText(Html.fromHtml(banner.getDescription()).toString());
                seriesLongDescriptionTextView.setText(Html.fromHtml(banner.getLongdescription()).toString());
                castTextView.setText(Html.fromHtml(banner.getCast()).toString());
                Picasso.with(DetailedListActivity.this).load("http://healthpro.in/vb/uploads/new/" + banner.getSeriesid() + ".jpg").into(showcasePosterImageView);
                initRating(banner.getSeriesid());
                onPlayButtonClicked();
                if (banner.getEpitype().equals("free")) {
                    isBinge = false;
                    paidDimmerFrameLayout.setBackgroundResource(0);
                    paytoBingImageView.setVisibility(View.GONE);
                    bingPriceTextView.setVisibility(View.GONE);
                    playToWatchTailerTextView.setVisibility(View.GONE);
                    playTrailerImageView.setVisibility(View.GONE);
                    ratingBar.setIsIndicator(false);
                    Drawable drawable = ratingBar.getProgressDrawable();
                    drawable.setColorFilter(Color.parseColor("#ee1d51"), PorterDuff.Mode.SRC_ATOP);

                } else {
                    isBinge = true;

                    SharedPreferences userSharedPreferences = getSharedPreferences(Constants.USER_PREF, MODE_PRIVATE);
                    String USER_COUNTRY = userSharedPreferences.getString(Constants.USER_PREF_USER_COUNTRY_KEY, "US");

                    String currency;
                    switch (USER_COUNTRY) {

                        case "IND":
                            currency = mContext.getResources().getString(R.string.rupee_symbol);
                            bingPriceTextView.setText(currency + " " + banner.getPrice());
                            break;
                        case "PAK":
                            currency = "PKR";
                            bingPriceTextView.setText(currency + " " + banner.getPakinr());
                            break;
                        case "BENG":
                            currency = mContext.getResources().getString(R.string.bengali_symbol);
                            bingPriceTextView.setText(currency + " " + banner.getBanginr());
                            break;
                        case "UK":
                            currency = mContext.getResources().getString(R.string.pound_symbol);
                            bingPriceTextView.setText(currency + " " + banner.getPound());
                            break;
                        case "US":
                            currency = mContext.getResources().getString(R.string.dollar_symbol);
                            bingPriceTextView.setText(currency + " " + banner.getDollar());
                            break;
                    }

                }
                bundle.putParcelable("banners", banner);
            }
            getVideosData();
        }

        if (getIntent().hasExtra("webseries")) {
            webseries = getIntent().getExtras().getParcelable("webseries");

            if (webseries != null) {
                ((TextView) toolbar.findViewById(R.id.toolbar_text_title)).setText(webseries.getTitle().trim().toUpperCase());
                ((TextView) toolbar.findViewById(R.id.toolbar_text_subtitle)).setText(Html.fromHtml(webseries.getDescription()).toString());

                seriesLongDescriptionTextView.setText(Html.fromHtml(webseries.getLongdescription()).toString());
                Picasso.with(DetailedListActivity.this).load("http://healthpro.in/vb/uploads/new/" + webseries.getSeriesid() + ".jpg").into(showcasePosterImageView);
                initRating(webseries.getSeriesid());
                castTextView.setText(Html.fromHtml(webseries.getCast()).toString());
                onPlayButtonClicked();
                if (webseries.getEpitype().equals("free")) {
                    isBinge = false;
                    paidDimmerFrameLayout.setBackgroundResource(0);
                    paytoBingImageView.setVisibility(View.GONE);
                    bingPriceTextView.setVisibility(View.GONE);
                    playToWatchTailerTextView.setVisibility(View.GONE);
                    playTrailerImageView.setVisibility(View.GONE);
                    ratingBar.setIsIndicator(false);
                    Drawable drawable = ratingBar.getProgressDrawable();
                    drawable.setColorFilter(Color.parseColor("#ee1d51"), PorterDuff.Mode.SRC_ATOP);


                    if (webseries.getTelugufolderid() != null || webseries.getTelugufolderid() != null) {

                        if (webseries.getTelugufolderid() == null) {
                            lang.add("Hindi");
                            lang.add("Tamil");
                        } else if (webseries.getTelugufolderid() == null) {
                            lang.add("Hindi");
                            lang.add("Telugu");
                        } else {
                            lang.add("Hindi");
                            lang.add("Tamil");
                            lang.add("Telugu");
                        }
                    } else {
                        lang.add("Hindi");
                    }

                } else {
                    isBinge = true;

                    SharedPreferences userSharedPreferences = getSharedPreferences(Constants.USER_PREF, MODE_PRIVATE);
                    String USER_COUNTRY = userSharedPreferences.getString(Constants.USER_PREF_USER_COUNTRY_KEY, "US");

                    String currency;
                    switch (USER_COUNTRY) {

                        case "IND":
                            currency = mContext.getResources().getString(R.string.rupee_symbol);
                            bingPriceTextView.setText(currency + " " + webseries.getPrice());
                            break;
                        case "PAK":
                            currency = "PKR";
                            bingPriceTextView.setText(currency + " " + webseries.getPakinr());
                            break;
                        case "BENG":
                            currency = mContext.getResources().getString(R.string.bengali_symbol);
                            bingPriceTextView.setText(currency + " " + webseries.getBanginr());
                            break;
                        case "UK":
                            currency = mContext.getResources().getString(R.string.pound_symbol);
                            bingPriceTextView.setText(currency + " " + webseries.getPound());
                            break;
                        case "US":
                            currency = mContext.getResources().getString(R.string.dollar_symbol);
                            bingPriceTextView.setText(currency + " " + webseries.getDollar());
                            break;
                    }
                    bundle.putParcelable("webseries", webseries);

                    if (webseries.getTelugufolderid() != null || webseries.getTelugufolderid() != null) {

                        if (webseries.getTelugufolderid() == null) {
                            lang.add("Hindi");
                            lang.add("Tamil");
                        } else if (webseries.getTelugufolderid() == null) {
                            lang.add("Hindi");
                            lang.add("Telugu");
                        } else {
                            lang.add("Hindi");
                            lang.add("Tamil");
                            lang.add("Telugu");
                        }
                    } else {
                        lang.add("Hindi");
                    }

                }
                getVideosData();
            }

            if (getIntent().hasExtra("user")) {
                user = getIntent().getExtras().getParcelable("user");
                bundle.putParcelable("user", user);
            }
        }
    }

    private void setRecyclerViews(List<Video> copyVideo) {

        //qq    episodeRecyclerView.setVisibility(View.VISIBLE);
        // qq        progressBar.setVisibility(View.GONE);

        progressDialog.dismiss();

        totalVideoTextView.setText(String.valueOf(copyVideo.size()) + " VIDEOS");
        showcaseEpisodesNumberTextView.setText(String.valueOf(copyVideo.size()));
        episodeRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        if (isBinge) {

            if (isBanner) {

                paidCheck(banner.getSeriesid(), copyVideo);
            } else {
                paidCheck(webseries.getSeriesid(), copyVideo);
            }

        } else {

            VerticalEpisodeAdapter verticalEpisodeAdapter = new VerticalEpisodeAdapter(this, copyVideo, true);
            verticalEpisodeAdapter.setOnClickListener(new VerticalEpisodeAdapter.OnClickListener() {
                @Override
                public void onClick(int position) {
                    Intent i = new Intent(DetailedListActivity.this, PlayerActivity.class);
                    if (webseries != null) {
                        i.putExtra("webseries", bundle.getParcelable("webseries"));
                        i.putExtra("startPoint", position);
                        i.putStringArrayListExtra("playlistArray", playlistVideo);
                        startActivity(i);
                    } else {
                        i.putExtra("banners", bundle.getParcelable("banners"));
                        i.putExtra("startPoint", position);
                        i.putStringArrayListExtra("playlistArray", playlistVideo);
                        startActivity(i);
                    }
                }
            });
            episodeRecyclerView.setAdapter(verticalEpisodeAdapter);
            verticalEpisodeAdapter.notifyDataSetChanged();
            //qq progressBar.setVisibility(View.GONE);
            progressDialog.dismiss();

            Log.d("BBING", "BING-F: " + isBinge);


        }


    }


    private void setRating() {

        seriesRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRatingDialog();

            }
        });
    }

    private void showRatingDialog() {

        final Dialog rankDialog = new Dialog(DetailedListActivity.this, R.style.FullHeightDialog);
        rankDialog.setContentView(R.layout.rank_dialog);
        rankDialog.setCancelable(true);
        ratingBar = (RatingBar) rankDialog.findViewById(R.id.dialog_ratingbar);

        TextView text = (TextView) rankDialog.findViewById(R.id.rank_dialog_text1);
        Button updateButton = (Button) rankDialog.findViewById(R.id.rank_dialog_button);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rankDialog.dismiss();
                onRatingChange(ratingBar.getRating());
            }
        });
        rankDialog.show();

    }


    private void onRatingChange(final Float changedRating) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "updtaterating",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("MTAG", String.valueOf(Math.round(changedRating)));
                        Toast.makeText(mContext, "Done", Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("rating", String.valueOf(Math.round(changedRating)));
                params.put("userid", Constants.getUserId());
                params.put("webseriesid", webseries.getSeriesid());
                return params;
            }
        };
        VolleySingleton.getInstance(DetailedListActivity.this).addToRequestQueue(stringRequest);

    }


    private void setupToolbarMenu() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void getVideosData() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                new AccessToken(mContext).execute();
            }

            @Override
            protected Void doInBackground(Void... voids) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                    }
                });
                getShowcaseVideo();
                //getVideoData();
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);


    }

    private void getShowcaseVideo() {

        //"https://cms.api.brightcove.com/v1/accounts/5546369565001/folders/" + folderId + "/videos"

        //episodeRecyclerView.setVisibility(View.GONE);
        //q progressBar.setVisibility(View.VISIBLE);

        String folder = "";
        if (isBanner) {
            folder = banner.getFolderid();
        } else {
            if (selectedLang.equals("hindi")) {
                folder = webseries.getFolderid();
            } else if (selectedLang.equals("tamil")) {
                folder = webseries.getTamilfolderid();
            } else if (selectedLang.equals("telugu")) {
                folder = webseries.getTamilfolderid();
            }
        }

        Log.i("Aniket", "https://cms.api.brightcove.com/v1/accounts/5546369565001/playlists/" + folder + "/videos");

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://cms.api.brightcove.com/v1/accounts/5546369565001/playlists/" + folder + "/videos", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                videos.clear();

                Type type = new TypeToken<List<Video>>() {
                }.getType();
                videos = new Gson().fromJson(response, type);

                try {
                    if (videos != null) {
                        showcaseVideoId = videos.get(0).getId();


                        if (videos.size() > 1) {
                            setRecyclerViews(videos);
                            for (int i = 0; i < videos.size(); i++) {
                                playlistVideo.add(videos.get(i).getId());
                            }
                        }
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "onErrorResponse: " + error.getMessage());
                Log.e("ERROR", "onErrorResponse: " + "https://cms.api.brightcove.com/v1/accounts/5546369565001/playlists/" + (isBanner ? banner.getFolderid().trim() : webseries.getFolderid().trim()) + "/videos");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + getSharedPreferences(Constants.ACCESS_PREF, MODE_PRIVATE).getString(Constants.ACCESS_PREF_KEY, ""));
                return params;
            }
        };
        VolleySingleton.getInstance(DetailedListActivity.this).addToRequestQueue(stringRequest);
    }


    private void paidCheck(final String seriesId, final List<Video> copyVideo) {

        if (getSharedPreferences(Constants.USER_PREF, MODE_PRIVATE).getBoolean(Constants.USER_PREF_SKIP_KEY, true)) {

            VerticalEpisodeAdapter verticalEpisodeAdapter = new VerticalEpisodeAdapter(DetailedListActivity.this, copyVideo, false);
            episodeRecyclerView.setAdapter(verticalEpisodeAdapter);
            verticalEpisodeAdapter.notifyDataSetChanged();
            Log.d("BBING", "BING-T: " + isBinge);
            onPlayTrailerButtonClicked();
            //qq progressBar.setVisibility(View.GONE);
            progressDialog.dismiss();


        } else {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "seriespaid",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("ISPAID", response);


                            if (!response.contains("Not Paid")) {

                                isPaid = true;
                                paidDimmerFrameLayout.setBackgroundResource(0);
                                paytoBingImageView.setVisibility(View.GONE);
                                bingPriceTextView.setVisibility(View.GONE);
                                playToWatchTailerTextView.setVisibility(View.GONE);
                                playTrailerImageView.setVisibility(View.GONE);
                                ratingBar.setIsIndicator(false);
                                Drawable drawable = ratingBar.getProgressDrawable();
                                drawable.setColorFilter(Color.parseColor("#ee1d51"), PorterDuff.Mode.SRC_ATOP);


                                VerticalEpisodeAdapter verticalEpisodeAdapter = new VerticalEpisodeAdapter(DetailedListActivity.this, copyVideo, isPaid);

                                verticalEpisodeAdapter.setOnClickListener(new VerticalEpisodeAdapter.OnClickListener() {
                                    @Override
                                    public void onClick(int position) {
                                        Intent i = new Intent(DetailedListActivity.this, PlayerActivity.class);
                                        if (webseries != null) {
                                            i.putExtra("webseries", bundle.getParcelable("webseries"));
                                            i.putExtra("startPoint", position);
                                            i.putStringArrayListExtra("playlistArray", playlistVideo);
                                            startActivity(i);
                                        } else {
                                            i.putExtra("banners", bundle.getParcelable("banners"));
                                            i.putExtra("startPoint", position);
                                            i.putStringArrayListExtra("playlistArray", playlistVideo);
                                            startActivity(i);
                                        }
                                    }
                                });

                                episodeRecyclerView.setAdapter(verticalEpisodeAdapter);
                                verticalEpisodeAdapter.notifyDataSetChanged();
                                episodeRecyclerView.setVisibility(View.VISIBLE);
                                //qq progressBar.setVisibility(View.GONE);
                                progressDialog.dismiss();


                            } else {

                                isPaid = false;
                                VerticalEpisodeAdapter verticalEpisodeAdapter = new VerticalEpisodeAdapter(DetailedListActivity.this, copyVideo, isPaid);
                                episodeRecyclerView.setAdapter(verticalEpisodeAdapter);
                                verticalEpisodeAdapter.notifyDataSetChanged();
                                Log.d("BBING", "BING-T: " + isBinge);
                                onPlayTrailerButtonClicked();
                                //qq progressBar.setVisibility(View.GONE);
                                progressDialog.dismiss();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("userid", Constants.getUserId());    // TODO: 23-Sep-17
                    params.put("seriesid", seriesId);
                    return params;
                }
            };
            VolleySingleton.getInstance(DetailedListActivity.this).addToRequestQueue(stringRequest);


        }
    }


    @Override
    public void onBackPressed() {
        NavUtils.navigateUpFromSameTask(this);
    }

}


