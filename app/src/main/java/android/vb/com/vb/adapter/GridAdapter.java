package android.vb.com.vb.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.vb.com.vb.R;
import android.vb.com.vb.activity.DetailedListActivity;
import android.vb.com.vb.activity.PlayerActivity;
import android.vb.com.vb.pojo.TrailersANDSongs;
import android.vb.com.vb.pojo.Webseries;
import android.vb.com.vb.util.Constants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by SUJAN on 13-Sep-17.
 */

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.GridViewHolder> {

    public boolean isBing;
    public String isTrailerANDSong;
    LayoutInflater layoutInflater;
    private Context mContext;
    private int[] seriesImageArray = {R.drawable.rainpromo, R.drawable.gehraiyaan, R.drawable.mayaa, R.drawable.spotlight, R.drawable.twisted1};
    private String[] seriesNameArray = {"Rain", "Gehraiyaan", "Mayaa", "Spotlight", "Twisted"};
    private List<Webseries> webseriesList = Collections.emptyList();
    private List<TrailersANDSongs> trailersANDSongsList = Collections.emptyList();

    public GridAdapter(Context context, List<Webseries> webseries, Boolean isBing) {
        this.mContext = context;
        layoutInflater = LayoutInflater.from(context);
        this.isBing = isBing;
        this.webseriesList = webseries;
    }


    public GridAdapter(Context context, List<TrailersANDSongs> trailersANDSongs, String isTrailerandSong) {
        this.mContext = context;
        layoutInflater = LayoutInflater.from(context);
        this.isTrailerANDSong = isTrailerandSong;
        this.trailersANDSongsList = trailersANDSongs;
    }


    @Override
    public GridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.adapter_grid, parent, false);
        return new GridAdapter.GridViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GridViewHolder holder, int position) {

        if (isTrailerANDSong == null) {

            final Webseries finalWebseries = webseriesList.get(position);
            Picasso.with(mContext).load("http://healthpro.in/vb/public/webseries/" + finalWebseries.getSeriesid() + ".jpg").into(holder.seriesImageView);
            holder.seriesNameTextView.setText(finalWebseries.getTitle());
            holder.episodeDetailTextView.setText(Html.fromHtml(finalWebseries.getDescription()).toString());
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mContext.startActivity(new Intent(mContext, DetailedListActivity.class).putExtra("webseries", finalWebseries));
                }
            });
            if (isBing) {

                SharedPreferences userSharedPreferences = mContext.getSharedPreferences(Constants.USER_PREF, mContext.MODE_PRIVATE);
                String USER_COUNTRY = userSharedPreferences.getString(Constants.USER_PREF_USER_COUNTRY_KEY, "US");

                String currency;
                switch (USER_COUNTRY) {

                    case "IND":
                        currency = mContext.getResources().getString(R.string.rupee_symbol);
                        priceVisible(holder, finalWebseries.getPrice(), currency);
                        break;
                    case "PAK":
                        currency = "PKR";
                        priceVisible(holder, finalWebseries.getPakinr(), currency);
                        break;
                    case "BENG":
                        currency = mContext.getResources().getString(R.string.bengali_symbol);
                        priceVisible(holder, finalWebseries.getBanginr(), currency);
                        break;
                    case "UK":
                        currency = mContext.getResources().getString(R.string.pound_symbol);
                        priceVisible(holder, finalWebseries.getPound(), currency);
                        break;
                    case "US":
                        currency = mContext.getResources().getString(R.string.dollar_symbol);
                        priceVisible(holder, finalWebseries.getDollar(), currency);
                        break;
                }
            }
        } else {


            Log.d("YES", "YES");
            final TrailersANDSongs finalTrailersANDSongs = trailersANDSongsList.get(position);
            Picasso.with(mContext).load("http://healthpro.in/vb/public/webseries/" + finalTrailersANDSongs.getSeriesid() + ".jpg").into(holder.seriesImageView);
            holder.seriesNameTextView.setText(finalTrailersANDSongs.getTitle());
            holder.episodeDetailTextView.setText(Html.fromHtml(finalTrailersANDSongs.getDescription()).toString());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(mContext, PlayerActivity.class)
                            .putExtra("isSingleVideo", true)
                            .putExtra("videoId", finalTrailersANDSongs.getVideoid()));
                    //.putExtra("timestamp",finalTrailersANDSongs.getLength()));
                }
            });


        }

    }

    @Override
    public int getItemCount() {

        if (isTrailerANDSong == null) {
            return webseriesList.size();
        } else {
            return trailersANDSongsList.size();
        }
    }

    public void priceVisible(GridAdapter.GridViewHolder gridViewHolder, String price, String currency) {
        gridViewHolder.view.findViewById(R.id.adapter_grid_price_frameLayout).setVisibility(View.VISIBLE);
        gridViewHolder.priceTextView.setText(currency + " " + price);
    }


    class GridViewHolder extends RecyclerView.ViewHolder {

        View view;
        ImageView seriesImageView;
        TextView seriesNameTextView, newIndicatorTextView, episodeDetailTextView, priceTextView;

        public GridViewHolder(View itemView) {
            super(itemView);

            view = itemView;
            seriesImageView = (ImageView) itemView.findViewById(R.id.adapter_grid_serialImageView);
            seriesNameTextView = (TextView) itemView.findViewById(R.id.adapter_grid_serialNameTextView);
            newIndicatorTextView = (TextView) itemView.findViewById(R.id.adapter_grid_newIndicatorTextView);
            episodeDetailTextView = (TextView) itemView.findViewById(R.id.adapter_grid_episodeDetailTextView);
            priceTextView = (TextView) itemView.findViewById(R.id.adapter_grid_price_textView);


        }
    }


}
