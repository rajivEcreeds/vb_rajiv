package android.vb.com.vb.pojo;

/**
 * Created by SUJAN on 05-Sep-17.
 */

public class Episode {


    /**
     * episodeid : 2
     * seriesid : 3
     * title : Twisted Poster First
     * description : Twisted Poster First Description
     * episode : 1
     * epitype : epis
     * price : 0
     * episodedate : 2016-08-14
     * season : 4
     * uploaddate : 2017-06-22
     * videoname : 5560938304001
     * image : Twisted POSTER horizontal final (2).jpg
     * status : 1
     */

    private String episodeid;
    private String seriesid;
    private String title;
    private String description;
    private String episode;
    private String epitype;
    private String price;
    private String episodedate;
    private String season;
    private String uploaddate;
    private String videoname;
    private String image;
    private String status;

    public String getEpisodeid() {
        return episodeid;
    }

    public void setEpisodeid(String episodeid) {
        this.episodeid = episodeid;
    }

    public String getSeriesid() {
        return seriesid;
    }

    public void setSeriesid(String seriesid) {
        this.seriesid = seriesid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }

    public String getEpitype() {
        return epitype;
    }

    public void setEpitype(String epitype) {
        this.epitype = epitype;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getEpisodedate() {
        return episodedate;
    }

    public void setEpisodedate(String episodedate) {
        this.episodedate = episodedate;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getUploaddate() {
        return uploaddate;
    }

    public void setUploaddate(String uploaddate) {
        this.uploaddate = uploaddate;
    }

    public String getVideoname() {
        return videoname;
    }

    public void setVideoname(String videoname) {
        this.videoname = videoname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
