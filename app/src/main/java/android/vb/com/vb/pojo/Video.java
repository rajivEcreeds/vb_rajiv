package android.vb.com.vb.pojo;

import java.util.List;

/**
 * Created by SUJAN on 04-Sep-17.
 */

public class Video {


    /**
     * id : 5549656684001
     * account_id : 5546369565001
     * ad_keys : null
     * clip_source_video_id : null
     * complete : true
     * created_at : 2017-08-23T20:00:56.209Z
     * cue_points : []
     * custom_fields : {"description":"Nooo","paid":"yes"}
     * delivery_type : static_origin
     * description : hgf
     * digital_master_id : 5549666304001
     * duration : 122987
     * economics : AD_SUPPORTED
     * folder_id : 59a9149110ab34093b2adada
     * geo : null
     * has_digital_master : true
     * images : {"thumbnail":{"asset_id":"5549666401001","remote":false,"src":"http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666401001_5549656684001-th.jpg?pubId=5546369565001&videoId=5549656684001","sources":[{"src":"http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666401001_5549656684001-th.jpg?pubId=5546369565001&videoId=5549656684001","height":90,"width":160},{"src":"https://brightcove.hs.llnwd.net/e1/pd/5546369565001/5546369565001_5549666401001_5549656684001-th.jpg?pubId=5546369565001&videoId=5549656684001","height":90,"width":160}]},"poster":{"asset_id":"5549666302001","remote":false,"src":"http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666302001_5549656684001-vs.jpg?pubId=5546369565001&videoId=5549656684001","sources":[{"src":"http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666302001_5549656684001-vs.jpg?pubId=5546369565001&videoId=5549656684001","height":720,"width":1280},{"src":"https://brightcove.hs.llnwd.net/e1/pd/5546369565001/5546369565001_5549666302001_5549656684001-vs.jpg?pubId=5546369565001&videoId=5549656684001","height":720,"width":1280}]}}
     * link : null
     * long_description : Rain Promo
     * name : empty string
     * original_filename : RAIN_PROMO.mp4
     * projection : null
     * published_at : 2017-08-23T20:00:56.209Z
     * reference_id : 5549656684001
     * schedule : null
     * sharing : null
     * state : ACTIVE
     * tags : ["fghfg"]
     * text_tracks : []
     * updated_at : 2017-09-01T11:52:01.975Z
     */

    private String id;
    private String account_id;
    private Object ad_keys;
    private Object clip_source_video_id;
    private boolean complete;
    private String created_at;
    private CustomFieldsBean custom_fields;
    private String delivery_type;
    private String description;
    private String digital_master_id;
    private int duration;
    private String economics;
    private String folder_id;
    private Object geo;
    private boolean has_digital_master;
    private ImagesBean images;
    private Object link;
    private String long_description;
    private String name;
    private String original_filename;
    private Object projection;
    private String published_at;
    private String reference_id;
    private Object schedule;
    private Object sharing;
    private String state;
    private String updated_at;
    private List<?> cue_points;
    private List<String> tags;
    private List<?> text_tracks;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public Object getAd_keys() {
        return ad_keys;
    }

    public void setAd_keys(Object ad_keys) {
        this.ad_keys = ad_keys;
    }

    public Object getClip_source_video_id() {
        return clip_source_video_id;
    }

    public void setClip_source_video_id(Object clip_source_video_id) {
        this.clip_source_video_id = clip_source_video_id;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public CustomFieldsBean getCustom_fields() {
        return custom_fields;
    }

    public void setCustom_fields(CustomFieldsBean custom_fields) {
        this.custom_fields = custom_fields;
    }

    public String getDelivery_type() {
        return delivery_type;
    }

    public void setDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDigital_master_id() {
        return digital_master_id;
    }

    public void setDigital_master_id(String digital_master_id) {
        this.digital_master_id = digital_master_id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getEconomics() {
        return economics;
    }

    public void setEconomics(String economics) {
        this.economics = economics;
    }

    public String getFolder_id() {
        return folder_id;
    }

    public void setFolder_id(String folder_id) {
        this.folder_id = folder_id;
    }

    public Object getGeo() {
        return geo;
    }

    public void setGeo(Object geo) {
        this.geo = geo;
    }

    public boolean isHas_digital_master() {
        return has_digital_master;
    }

    public void setHas_digital_master(boolean has_digital_master) {
        this.has_digital_master = has_digital_master;
    }

    public ImagesBean getImages() {
        return images;
    }

    public void setImages(ImagesBean images) {
        this.images = images;
    }

    public Object getLink() {
        return link;
    }

    public void setLink(Object link) {
        this.link = link;
    }

    public String getLong_description() {
        return long_description;
    }

    public void setLong_description(String long_description) {
        this.long_description = long_description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOriginal_filename() {
        return original_filename;
    }

    public void setOriginal_filename(String original_filename) {
        this.original_filename = original_filename;
    }

    public Object getProjection() {
        return projection;
    }

    public void setProjection(Object projection) {
        this.projection = projection;
    }

    public String getPublished_at() {
        return published_at;
    }

    public void setPublished_at(String published_at) {
        this.published_at = published_at;
    }

    public String getReference_id() {
        return reference_id;
    }

    public void setReference_id(String reference_id) {
        this.reference_id = reference_id;
    }

    public Object getSchedule() {
        return schedule;
    }

    public void setSchedule(Object schedule) {
        this.schedule = schedule;
    }

    public Object getSharing() {
        return sharing;
    }

    public void setSharing(Object sharing) {
        this.sharing = sharing;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public List<?> getCue_points() {
        return cue_points;
    }

    public void setCue_points(List<?> cue_points) {
        this.cue_points = cue_points;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<?> getText_tracks() {
        return text_tracks;
    }

    public void setText_tracks(List<?> text_tracks) {
        this.text_tracks = text_tracks;
    }

    public static class CustomFieldsBean {
        /**
         * description : Nooo
         * paid : yes
         */

        private String description;
        private String paid;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPaid() {
            return paid;
        }

        public void setPaid(String paid) {
            this.paid = paid;
        }
    }

    public static class ImagesBean {
        /**
         * thumbnail : {"asset_id":"5549666401001","remote":false,"src":"http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666401001_5549656684001-th.jpg?pubId=5546369565001&videoId=5549656684001","sources":[{"src":"http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666401001_5549656684001-th.jpg?pubId=5546369565001&videoId=5549656684001","height":90,"width":160},{"src":"https://brightcove.hs.llnwd.net/e1/pd/5546369565001/5546369565001_5549666401001_5549656684001-th.jpg?pubId=5546369565001&videoId=5549656684001","height":90,"width":160}]}
         * poster : {"asset_id":"5549666302001","remote":false,"src":"http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666302001_5549656684001-vs.jpg?pubId=5546369565001&videoId=5549656684001","sources":[{"src":"http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666302001_5549656684001-vs.jpg?pubId=5546369565001&videoId=5549656684001","height":720,"width":1280},{"src":"https://brightcove.hs.llnwd.net/e1/pd/5546369565001/5546369565001_5549666302001_5549656684001-vs.jpg?pubId=5546369565001&videoId=5549656684001","height":720,"width":1280}]}
         */

        private ThumbnailBean thumbnail;
        private PosterBean poster;

        public ThumbnailBean getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(ThumbnailBean thumbnail) {
            this.thumbnail = thumbnail;
        }

        public PosterBean getPoster() {
            return poster;
        }

        public void setPoster(PosterBean poster) {
            this.poster = poster;
        }

        public static class ThumbnailBean {
            /**
             * asset_id : 5549666401001
             * remote : false
             * src : http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666401001_5549656684001-th.jpg?pubId=5546369565001&videoId=5549656684001
             * sources : [{"src":"http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666401001_5549656684001-th.jpg?pubId=5546369565001&videoId=5549656684001","height":90,"width":160},{"src":"https://brightcove.hs.llnwd.net/e1/pd/5546369565001/5546369565001_5549666401001_5549656684001-th.jpg?pubId=5546369565001&videoId=5549656684001","height":90,"width":160}]
             */

            private String asset_id;
            private boolean remote;
            private String src;
            private List<SourcesBean> sources;

            public String getAsset_id() {
                return asset_id;
            }

            public void setAsset_id(String asset_id) {
                this.asset_id = asset_id;
            }

            public boolean isRemote() {
                return remote;
            }

            public void setRemote(boolean remote) {
                this.remote = remote;
            }

            public String getSrc() {
                return src;
            }

            public void setSrc(String src) {
                this.src = src;
            }

            public List<SourcesBean> getSources() {
                return sources;
            }

            public void setSources(List<SourcesBean> sources) {
                this.sources = sources;
            }

            public static class SourcesBean {
                /**
                 * src : http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666401001_5549656684001-th.jpg?pubId=5546369565001&videoId=5549656684001
                 * height : 90
                 * width : 160
                 */

                private String src;
                private int height;
                private int width;

                public String getSrc() {
                    return src;
                }

                public void setSrc(String src) {
                    this.src = src;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }
            }
        }

        public static class PosterBean {
            /**
             * asset_id : 5549666302001
             * remote : false
             * src : http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666302001_5549656684001-vs.jpg?pubId=5546369565001&videoId=5549656684001
             * sources : [{"src":"http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666302001_5549656684001-vs.jpg?pubId=5546369565001&videoId=5549656684001","height":720,"width":1280},{"src":"https://brightcove.hs.llnwd.net/e1/pd/5546369565001/5546369565001_5549666302001_5549656684001-vs.jpg?pubId=5546369565001&videoId=5549656684001","height":720,"width":1280}]
             */

            private String asset_id;
            private boolean remote;
            private String src;
            private List<SourcesBeanX> sources;

            public String getAsset_id() {
                return asset_id;
            }

            public void setAsset_id(String asset_id) {
                this.asset_id = asset_id;
            }

            public boolean isRemote() {
                return remote;
            }

            public void setRemote(boolean remote) {
                this.remote = remote;
            }

            public String getSrc() {
                return src;
            }

            public void setSrc(String src) {
                this.src = src;
            }

            public List<SourcesBeanX> getSources() {
                return sources;
            }

            public void setSources(List<SourcesBeanX> sources) {
                this.sources = sources;
            }

            public static class SourcesBeanX {
                /**
                 * src : http://brightcove.vo.llnwd.net/e1/pd/5546369565001/5546369565001_5549666302001_5549656684001-vs.jpg?pubId=5546369565001&videoId=5549656684001
                 * height : 720
                 * width : 1280
                 */

                private String src;
                private int height;
                private int width;

                public String getSrc() {
                    return src;
                }

                public void setSrc(String src) {
                    this.src = src;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

            }
        }
    }


    @Override
    public String toString() {
        return "Video{" +
                "id='" + id + '\'' +
                ", account_id='" + account_id + '\'' +
                ", ad_keys=" + ad_keys +
                ", clip_source_video_id=" + clip_source_video_id +
                ", complete=" + complete +
                ", created_at='" + created_at + '\'' +
                ", custom_fields=" + custom_fields +
                ", delivery_type='" + delivery_type + '\'' +
                ", description='" + description + '\'' +
                ", digital_master_id='" + digital_master_id + '\'' +
                ", duration=" + duration +
                ", economics='" + economics + '\'' +
                ", folder_id='" + folder_id + '\'' +
                ", geo=" + geo +
                ", has_digital_master=" + has_digital_master +
                ", images=" + images +
                ", link=" + link +
                ", long_description='" + long_description + '\'' +
                ", name='" + name + '\'' +
                ", original_filename='" + original_filename + '\'' +
                ", projection=" + projection +
                ", published_at='" + published_at + '\'' +
                ", reference_id='" + reference_id + '\'' +
                ", schedule=" + schedule +
                ", sharing=" + sharing +
                ", state='" + state + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", cue_points=" + cue_points +
                ", tags=" + tags +
                ", text_tracks=" + text_tracks +
                '}';
    }
}
