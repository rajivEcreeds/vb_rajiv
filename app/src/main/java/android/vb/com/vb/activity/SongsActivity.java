package android.vb.com.vb.activity;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.vb.com.vb.R;
import android.vb.com.vb.adapter.GridAdapter;
import android.vb.com.vb.pojo.TrailersANDSongs;
import android.vb.com.vb.singleton.VolleySingleton;
import android.vb.com.vb.util.Constants;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SongsActivity extends AppCompatActivity {


    private Toolbar toolbar;
    private RecyclerView songsRecyclerView;
    private TrailersANDSongs[] songs;
    private LinearLayout noDataLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs);


        initToolbar();
        init();
        initTrailers();
    }

    private void initToolbar() {

        toolbar = (Toolbar) findViewById(R.id.activity_songs_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Songs");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

    }

    private void init() {

        noDataLinearLayout = (LinearLayout) findViewById(R.id.activity_songs_grid_recycler_view_nodata_LinearLayout);
    }

    private void setSongsGridRecyclerView() {

        GridAdapter songGridAdapter = new GridAdapter(this, Arrays.asList(songs), "YES");
        songsRecyclerView = (RecyclerView) findViewById(R.id.activity_songs_grid_recycler_view_recyclerview);

        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        //String toastMsg;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                songsRecyclerView.setLayoutManager(new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                songsRecyclerView.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
                break;
            default:
                songsRecyclerView.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
        }


        //songsRecyclerView.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
        songsRecyclerView.setAdapter(songGridAdapter);
        songGridAdapter.notifyDataSetChanged();
        songsRecyclerView.setVisibility(View.VISIBLE);
    }


    private void initTrailers() {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "songs",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("GLOG", "SONG :  " + response);

                        if (response.contains("No Result Found")) {

                            noDataLinearLayout.setVisibility(View.VISIBLE);


                        } else {
                            songs = new Gson().fromJson(response, TrailersANDSongs[].class);

                            Log.d("GLOG", "SONGT :  " + songs[0].toString());
                            setSongsGridRecyclerView();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SongsActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        VolleySingleton.getInstance(SongsActivity.this).addToRequestQueue(stringRequest);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
