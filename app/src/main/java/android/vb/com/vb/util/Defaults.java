package android.vb.com.vb.util;

import android.app.Application;
import android.vb.com.vb.R;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;
import com.tozny.crypto.android.AesCbcWithIntegrity;

import java.security.GeneralSecurityException;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by SUJAN on 22-Aug-17.
 */

public class Defaults extends Application {

    public static AesCbcWithIntegrity.SecretKeys KEY;


    static {
        try {
            KEY = AesCbcWithIntegrity.generateKey();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
    }

    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;
    public static Bus bus;

    @Override
    public void onCreate() {
        super.onCreate();
        sAnalytics = GoogleAnalytics.getInstance(this);
        defaultFont();
        bus = new Bus(ThreadEnforcer.MAIN);
    }

    private void defaultFont() {

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public Bus getBus(){
        if (bus!=null){
            return bus;
        }
        return null;
    }

    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }

}
