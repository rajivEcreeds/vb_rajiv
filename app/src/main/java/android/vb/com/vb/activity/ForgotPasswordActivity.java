package android.vb.com.vb.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.vb.com.vb.R;
import android.vb.com.vb.singleton.VolleySingleton;
import android.vb.com.vb.util.Constants;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgotPasswordActivity extends AppCompatActivity {


    private EditText emailEditText;
    private Button sendEmailButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        init();
        onClick();

    }

    private void init() {

        emailEditText = (EditText) findViewById(R.id.activity_forgot_password_email_editText);
        sendEmailButton = (Button) findViewById(R.id.activity_forgot_password_loginButton);
    }

    private void onClick() {

        sendEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initForgotPassword();
            }
        });
    }

    private void initForgotPassword() {

        if (Constants.isEmailValid(emailEditText.getText().toString())) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "checkforgetpassword",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("GF", "onResponse: " + response);
                            if (response.contains("success")) {
                                Toast.makeText(ForgotPasswordActivity.this, "Request sent successfully", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(ForgotPasswordActivity.this, LandingActivity.class));
                            } else {
                                Toast.makeText(ForgotPasswordActivity.this, "Couldn't find Your Email ID!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(ForgotPasswordActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("email", String.valueOf(emailEditText.getText().toString()));
                    return params;
                }
            };
            VolleySingleton.getInstance(ForgotPasswordActivity.this).addToRequestQueue(stringRequest);
        } else {
            Toast.makeText(this, "Invalid Email", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
