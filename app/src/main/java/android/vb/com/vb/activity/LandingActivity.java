package android.vb.com.vb.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.vb.com.vb.R;
import android.vb.com.vb.singleton.VolleySingleton;
import android.vb.com.vb.util.Constants;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LandingActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener {

    private static final int GOOGLE_REQ_CODE = 211;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final int REQUEST_LOCATION = 202;

    private Button signUpButton, loginButton;
    private TextView skipTextView;
    private ProgressDialog loginProgressDialog;
    private ImageView googleSignImageView, facebookLoginImageView;

    private GoogleApiClient googleApiClient;
    private CallbackManager fbCallbackManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_landing);

        Constants.fbKeyHash(this);

        init();
        onClick();


        googleSignInClicked();
        fbLoginClicked();


        checkCountry();


        try {
            userLoggedInCheck();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }


    }


    private void init() {
        signUpButton = (Button) findViewById(R.id.activity_landing_singupButton);
        loginButton = (Button) findViewById(R.id.activity_landing_loginButton);
        //skipTextView = (TextView) findViewById(R.id.activity_landing_skipTextView);


        facebookLoginImageView = (ImageView) findViewById(R.id.activity_landing_fb_login_ImageView);
        googleSignImageView = (ImageView) findViewById(R.id.activity_landing_gPlus_login_ImageView);

        fbCallbackManager = CallbackManager.Factory.create();
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        /*googleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();*/


        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();


    }


    private void onClick() {

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LandingActivity.this, SignupActivity.class));
            }
        });


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LandingActivity.this, LoginActivity.class));

            }
        });

       /* skipTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences.Editor userPreferenceEditor = getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE).edit();
                userPreferenceEditor.putBoolean(Constants.USER_PREF_SKIP_KEY, true);
                userPreferenceEditor.apply();
                userPreferenceEditor.commit();
                startActivity(new Intent(LandingActivity.this, HomeActivity.class));

            }
        });*/
    }


    private void checkCountry() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"),
                Locale.getDefault());
        Date currentLocalTime = calendar.getTime();
        DateFormat date = new SimpleDateFormat("z", Locale.getDefault());
        String localTime = date.format(currentLocalTime);

        SharedPreferences.Editor userPreferenceEditor = getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE).edit();
        //USER_PREF_USER_COUNTRY_KEY

        switch (localTime) {

            case "IST":
                Constants.USER_COUNTRY = "IND";
                userPreferenceEditor.putString(Constants.USER_PREF_USER_COUNTRY_KEY, "IND");
                break;
            case "GMT+05:30":
                Constants.USER_COUNTRY = "IND";
                userPreferenceEditor.putString(Constants.USER_PREF_USER_COUNTRY_KEY, "IND");
                break;
            case "GMT+05:00":
                Constants.USER_COUNTRY = "PAK";
                userPreferenceEditor.putString(Constants.USER_PREF_USER_COUNTRY_KEY, "PAK");
                break;
            case "GMT+06:00":
                Constants.USER_COUNTRY = "BENG";
                userPreferenceEditor.putString(Constants.USER_PREF_USER_COUNTRY_KEY, "BENG");
                break;
            case "GMT+01:00":
                Constants.USER_COUNTRY = "UK";
                userPreferenceEditor.putString(Constants.USER_PREF_USER_COUNTRY_KEY, "UK");
                break;
            case "GMT":
                Constants.USER_COUNTRY = "UK";
                userPreferenceEditor.putString(Constants.USER_PREF_USER_COUNTRY_KEY, "UK");
                break;
            default:
                Constants.USER_COUNTRY = "US";
                userPreferenceEditor.putString(Constants.USER_PREF_USER_COUNTRY_KEY, "US");
                break;
        }
        userPreferenceEditor.apply();
        userPreferenceEditor.commit();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void userLoggedInCheck() throws UnsupportedEncodingException, GeneralSecurityException {

        SharedPreferences userSharedPreferences = getSharedPreferences(Constants.USER_PREF, MODE_PRIVATE);
        boolean isLoggedIn = userSharedPreferences.getBoolean(Constants.USER_PREF_LOGIN_KEY, false);
        boolean isSocialIn = userSharedPreferences.getBoolean(Constants.USER_PREF_SOCIAL_LOGIN_KEY, false);

        if (isLoggedIn) {

            /*loginProgressDialog = new ProgressDialog(this);
            //loginProgressDialog.setProgressStyle(R.style.ProgressBar);
            loginProgressDialog.setIndeterminate(true);
            loginProgressDialog.setMessage("Logging in...");

            loginProgressDialog.setCancelable(false);
            loginProgressDialog.show();*/


            loginProgressDialog = new ProgressDialog(this, R.style.MyTheme);
            //progressDialog.setMessage("Loading...");
            loginProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
            loginProgressDialog.setCancelable(false);
            loginProgressDialog.show();


            if (isSocialIn) {

                final String userEmail = userSharedPreferences.getString(Constants.USER_PREF_EMAIL_KEY, "");    // TODO: 23-Sep-17

                final String userFName = userSharedPreferences.getString(Constants.USER_PREF_NAME_KEY, "");
                final String userLName = userSharedPreferences.getString(Constants.USER_PREF_LAST_NAME_KEY, "");

                Log.d("GAAA", userFName + "\n" + userLName + "\n" + userEmail);
                initSocialLogin(userFName, userLName, userEmail);

            } else {

                final String userEmail = userSharedPreferences.getString(Constants.USER_PREF_EMAIL_KEY, "");    // TODO: 23-Sep-17

                final String userPassword = userSharedPreferences.getString(Constants.USER_PREF_PASSWORD_KEY, "");    // TODO: 23-Sep-17

                initLogin(userEmail, userPassword);
            }


        }
    }

    private void initLogin(final String email, final String password) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "login",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonRootObject = new JSONObject(response);
                            String message = jsonRootObject.optString("message");
                            String userid = jsonRootObject.optString("id");


                            if (message.equals("Success")) {

                                Constants.setUserId(userid);     // TODO: 23-Sep-17

                                Log.d("LANDUID", "onResponse: " + userid);

                                // loginProgressDialog.dismiss();//// TODO: 05-Oct-17
                                    /*SharedPreferences.Editor userPreferenceEditor = getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE).edit();
                                    userPreferenceEditor.putString(Constants.USER_PREF_EMAIL_KEY, Constants.encryptThis(LandingActivity.this, emailEditText.getText().toString()));
                                    userPreferenceEditor.putString(Constants.USER_PREF_PASSWORD_KEY, Constants.encryptThis(LandingActivity.this, passwordEditText.getText().toString()));
                                    userPreferenceEditor.apply();
                                    userPreferenceEditor.commit();*/


                                startActivity(new Intent(LandingActivity.this, HomeActivity.class));
                            } else if (message.equals("Username or Password Wrong")) {

                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(LandingActivity.this, "Incorrect username or password", Toast.LENGTH_LONG).show();
                                        loginProgressDialog.dismiss();
                                    }
                                });
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LandingActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", Constants.md5(password));
                params.put("logintime", Calendar.getInstance().getTimeZone().getDisplayName(false, TimeZone.SHORT));
                params.put("ip_address", Constants.getIPAddress(false));
                return params;
            }

        };
        VolleySingleton.getInstance(LandingActivity.this).addToRequestQueue(stringRequest);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fbCallbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handlerResult(result);
        }
    }


    private void displayUserFbInfo(JSONObject jsonObject) {

        String first_name = "";
        String last_name = "";
        String email = "";
        String id = "";
        String profilePicUrl = "";

        try {
            first_name = jsonObject.getString("first_name");
            last_name = jsonObject.getString("last_name");
            email = jsonObject.getString("email");
            id = jsonObject.getString("id");

            profilePicUrl = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");

            initSocialLogin(first_name, last_name, email);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("GFBB", "fName: " + first_name
                + "\nlName: " + last_name
                + "\nemail: " + email
                + "\npicture: " + profilePicUrl
                + "\nid: " + id);
    }


    private void googleSignInClicked() {
        googleSignImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent, GOOGLE_REQ_CODE);
            }
        });
    }

    private void fbLoginClicked() {
        facebookLoginImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();
                    initFb();
                } else {
                    initFb();
                }


            }
        });
    }

    private void handlerResult(GoogleSignInResult googleSignInResult) {

        if (googleSignInResult.isSuccess()) {
            GoogleSignInAccount googleSignInAccount = googleSignInResult.getSignInAccount();
            String[] name = googleSignInAccount.getDisplayName().split(" ");
            String fname = name[0];
            String lname = name[1];
            String email = googleSignInAccount.getEmail();
            //String img_url = googleSignInAccount.getPhotoUrl().toString();

            initSocialLogin(fname, lname, email);

            Log.d("GGLOG", "fName: " + fname + "\nLName: " + lname + "\nEmail: " + email + "\nPic: ");
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }

    }


    private void initSocialLogin(final String fName, final String lName, final String email) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "sociallogin",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {

                            JSONObject jsonRootObject = new JSONObject(response);

                            String time = android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss a", new java.util.Date()).toString();
                            String userid = jsonRootObject.optString("id");
                            Log.d("GLAND", "onResponse: " + ("email" + email)
                                    + "\n" + ("name" + fName)
                                    + "\n" + ("lastname" + lName)
                                    + "\n" + ("ip_address" + Constants.getIPAddress(false))
                                    + "\n" + ("logintime" + time.substring(0, time.length() - 2)));


                            if (!response.contains("Some Details Missing")) {

                                Constants.setUserId(userid);     // TODO: 23-Sep-17

                                SharedPreferences.Editor userPreferenceEditor = getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE).edit();
                                userPreferenceEditor.putBoolean(Constants.USER_PREF_LOGIN_KEY, true);
                                userPreferenceEditor.putBoolean(Constants.USER_PREF_SOCIAL_LOGIN_KEY, true);
                                userPreferenceEditor.putString(Constants.USER_PREF_EMAIL_KEY, email);     // TODO: 23-Sep-17
                                userPreferenceEditor.putBoolean(Constants.USER_PREF_SKIP_KEY, false);
                                userPreferenceEditor.apply();
                                userPreferenceEditor.commit();


                                startActivity(new Intent(LandingActivity.this, HomeActivity.class));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LandingActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String time = android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss a", new java.util.Date()).toString();
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("name", fName);
                params.put("lastname", lName);
                params.put("ip_address", Constants.getIPAddress(false));
                params.put("logintime", time.substring(0, time.length() - 2));
                return params;
            }
        };
        VolleySingleton.getInstance(LandingActivity.this).addToRequestQueue(stringRequest);
    }


    private void initFb() {

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(fbCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("GFB", "onSuccess: " + loginResult.getAccessToken());
                String userEmailId = loginResult.getAccessToken().getUserId();

                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        displayUserFbInfo(object);
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name,last_name,email,id,cover,picture.type(large)");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d("GFB", "onCancel: ");

            }

            @Override
            public void onError(FacebookException error) {
                Log.d("GFB", "onError" + error.toString());
            }
        });
    }


}
