package android.vb.com.vb.activity;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.vb.com.vb.R;
import android.vb.com.vb.adapter.GridAdapter;
import android.vb.com.vb.pojo.TrailersANDSongs;
import android.vb.com.vb.singleton.VolleySingleton;
import android.vb.com.vb.util.Constants;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TrailersActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView trailerRecyclerView;
    private TrailersANDSongs[] trailers;
    private LinearLayout noDataLinearLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trailers);

        initToolbar();
        init();
        initTrailers();

    }


    private void initToolbar() {

        toolbar = (Toolbar) findViewById(R.id.activity_trailers_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Trailers");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

    }

    private void init() {

        noDataLinearLayout = (LinearLayout) findViewById(R.id.activity_trailers_grid_recycler_view_nodata_LinearLayout);
    }


    private void setTrailerGridRecyclerView() {

        GridAdapter trailerGridAdapter = new GridAdapter(this, Arrays.asList(trailers), "YES");
        trailerRecyclerView = (RecyclerView) findViewById(R.id.activity_trailers_grid_recycler_view_recyclerview);

        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        //String toastMsg;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                trailerRecyclerView.setLayoutManager(new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                trailerRecyclerView.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
                break;
            default:
                trailerRecyclerView.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
        }

        //trailerRecyclerView.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
        trailerRecyclerView.setAdapter(trailerGridAdapter);
        trailerGridAdapter.notifyDataSetChanged();
        trailerRecyclerView.setVisibility(View.VISIBLE);


    }


    private void initTrailers() {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "trailers",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("GLOG", "TRAILER :  " + response);

                        if (response.contains("No Result Found")) {

                            noDataLinearLayout.setVisibility(View.VISIBLE);


                        } else {

                            trailers = new Gson().fromJson(response, TrailersANDSongs[].class);
                            Log.d("GLOG", "TRAILERT :  " + trailers[0].toString());
                            setTrailerGridRecyclerView();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TrailersActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        VolleySingleton.getInstance(TrailersActivity.this).addToRequestQueue(stringRequest);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}

