package android.vb.com.vb.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by SUJAN on 05-Sep-17.
 */

public class Webseries implements Parcelable {


    public static final Creator<Webseries> CREATOR = new Creator<Webseries>() {
        @Override
        public Webseries createFromParcel(Parcel source) {
            return new Webseries(source);
        }

        @Override
        public Webseries[] newArray(int size) {
            return new Webseries[size];
        }
    };
    /**
     * seriesid : 4
     * title : MAAYA
     * description : Slave of her desires<br>
     * longdescription : Sonia is devastated by an unknown emotional trauma to slip into a coma. While her doctor husband Abhishek tried to uncover the truth. In the meanwhile, Rahul pays the price of a bad marriage. Maaya, a brand new erotic love story is filmmaker Vikram Bhatt's first web series directorial.<br>
     * generes : 12
     * cast : Shama Sikander, Vipul Gupta, Veer Aryan, Aradhya Taing, Parina Chopra
     * epitype : free
     * price : 0
     * pakinr : 0
     * banginr : 0
     * pound : 0
     * dollar : 0
     * folderid : 5564385782001
     * tamilfolderid :
     * telugufolderid :
     * createdate : 2017-09-18
     * image : MAAYA POSTER1 (1).jpg
     * status : 1
     * hinditrailer : null
     * tamiltrailer : null
     * telugutrailer : null
     */

    private String seriesid;
    private String title;
    private String description;
    private String longdescription;
    private String generes;
    private String cast;
    private String epitype;
    private String price;
    private String pakinr;
    private String banginr;
    private String pound;
    private String dollar;
    private String folderid;
    private String tamilfolderid;
    private String telugufolderid;
    private String createdate;
    private String image;
    private String status;
    private String hinditrailer;
    private String tamiltrailer;
    private String telugutrailer;

    public Webseries() {
    }

    protected Webseries(Parcel in) {
        this.seriesid = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.longdescription = in.readString();
        this.generes = in.readString();
        this.cast = in.readString();
        this.epitype = in.readString();
        this.price = in.readString();
        this.pakinr = in.readString();
        this.banginr = in.readString();
        this.pound = in.readString();
        this.dollar = in.readString();
        this.folderid = in.readString();
        this.tamilfolderid = in.readString();
        this.telugufolderid = in.readString();
        this.createdate = in.readString();
        this.image = in.readString();
        this.status = in.readString();
        this.hinditrailer = in.readString();
        this.tamiltrailer = in.readString();
        this.telugutrailer = in.readString();
    }

    public String getSeriesid() {
        return seriesid;
    }

    public void setSeriesid(String seriesid) {
        this.seriesid = seriesid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLongdescription() {
        return longdescription;
    }

    public void setLongdescription(String longdescription) {
        this.longdescription = longdescription;
    }

    public String getGeneres() {
        return generes;
    }

    public void setGeneres(String generes) {
        this.generes = generes;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getEpitype() {
        return epitype;
    }

    public void setEpitype(String epitype) {
        this.epitype = epitype;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPakinr() {
        return pakinr;
    }

    public void setPakinr(String pakinr) {
        this.pakinr = pakinr;
    }

    public String getBanginr() {
        return banginr;
    }

    public void setBanginr(String banginr) {
        this.banginr = banginr;
    }

    public String getPound() {
        return pound;
    }

    public void setPound(String pound) {
        this.pound = pound;
    }

    public String getDollar() {
        return dollar;
    }

    public void setDollar(String dollar) {
        this.dollar = dollar;
    }

    public String getFolderid() {
        return folderid;
    }

    public void setFolderid(String folderid) {
        this.folderid = folderid;
    }

    public String getTamilfolderid() {
        return tamilfolderid;
    }

    public void setTamilfolderid(String tamilfolderid) {
        this.tamilfolderid = tamilfolderid;
    }

    public String getTelugufolderid() {
        return telugufolderid;
    }

    public void setTelugufolderid(String telugufolderid) {
        this.telugufolderid = telugufolderid;
    }

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHinditrailer() {
        return hinditrailer;
    }

    public void setHinditrailer(String hinditrailer) {
        this.hinditrailer = hinditrailer;
    }

    public String getTamiltrailer() {
        return tamiltrailer;
    }

    public void setTamiltrailer(String tamiltrailer) {
        this.tamiltrailer = tamiltrailer;
    }

    public String getTelugutrailer() {
        return telugutrailer;
    }

    public void setTelugutrailer(String telugutrailer) {
        this.telugutrailer = telugutrailer;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.seriesid);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.longdescription);
        dest.writeString(this.generes);
        dest.writeString(this.cast);
        dest.writeString(this.epitype);
        dest.writeString(this.price);
        dest.writeString(this.pakinr);
        dest.writeString(this.banginr);
        dest.writeString(this.pound);
        dest.writeString(this.dollar);
        dest.writeString(this.folderid);
        dest.writeString(this.tamilfolderid);
        dest.writeString(this.telugufolderid);
        dest.writeString(this.createdate);
        dest.writeString(this.image);
        dest.writeString(this.status);
        dest.writeString(this.hinditrailer);
        dest.writeString(this.tamiltrailer);
        dest.writeString(this.telugutrailer);
    }

    @Override
    public String toString() {
        return "Webseries{" +
                "seriesid='" + seriesid + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", longdescription='" + longdescription + '\'' +
                ", generes='" + generes + '\'' +
                ", cast='" + cast + '\'' +
                ", epitype='" + epitype + '\'' +
                ", price='" + price + '\'' +
                ", pakinr='" + pakinr + '\'' +
                ", banginr='" + banginr + '\'' +
                ", pound='" + pound + '\'' +
                ", dollar='" + dollar + '\'' +
                ", folderid='" + folderid + '\'' +
                ", tamilfolderid='" + tamilfolderid + '\'' +
                ", telugufolderid='" + telugufolderid + '\'' +
                ", createdate='" + createdate + '\'' +
                ", image='" + image + '\'' +
                ", status='" + status + '\'' +
                ", hinditrailer='" + hinditrailer + '\'' +
                ", tamiltrailer='" + tamiltrailer + '\'' +
                ", telugutrailer='" + telugutrailer + '\'' +
                '}';
    }
}