package android.vb.com.vb.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.vb.com.vb.R;
import android.vb.com.vb.activity.DetailedListActivity;
import android.vb.com.vb.pojo.Banner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by SUJAN on 08-Sep-17.
 */

public class BannerAdapter extends PagerAdapter {


    private List<Banner> bannerList = Collections.emptyList();
    private LayoutInflater layoutInflater;
    private Context mContext;


    public BannerAdapter(Context context, List<Banner> banner) {
        this.mContext = context;
        this.bannerList = banner;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return bannerList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {

        View containerView = layoutInflater.inflate(R.layout.adapter_banner, view, false);
        final Banner finalBanner = bannerList.get(position);
        ImageView bannerImageView = (ImageView) containerView.findViewById(R.id.image);
        Picasso.with(mContext).load("http://healthpro.in/vb/public/sliders/" + finalBanner.getSlidername()).into(bannerImageView);
        if (finalBanner.getFolderid() != null) {
            containerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(mContext, DetailedListActivity.class).putExtra("banners", finalBanner));
                }
            });
        }
        view.addView(containerView, 0);
        return containerView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}
