package android.vb.com.vb.util;

/**
 * Created by SUJAN on 19-Sep-17.
 */


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.brightcove.player.event.Default;
import com.brightcove.player.event.Event;
import com.brightcove.player.event.EventListener;
import com.brightcove.player.event.EventLogger;
import com.brightcove.player.util.LifecycleUtil;
import com.brightcove.player.view.BaseVideoView;

public class BrightcovePlayerActivity extends AppCompatActivity {
    public static final String TAG = BrightcovePlayerActivity.class.getSimpleName();
    protected BaseVideoView baseVideoView;
    private EventLogger eventLogger;
    private LifecycleUtil lifecycleUtil;
    private Bundle savedInstanceState;

    public BrightcovePlayerActivity() {
    }

    public BaseVideoView getBaseVideoView() {
        return this.baseVideoView;
    }

    public void showClosedCaptioningDialog() {
        this.baseVideoView.getClosedCaptioningController().showCaptionsDialog();
    }

    public void enterFullScreen() {
        this.baseVideoView.getEventEmitter().emit("enterFullScreen");
    }

    public void exitFullScreen() {
        this.baseVideoView.getEventEmitter().emit("exitFullScreen");
    }

    public EventLogger getEventLogger() {
        return this.eventLogger;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(this.baseVideoView == null || this.lifecycleUtil != null && this.lifecycleUtil.baseVideoView == this.baseVideoView) {
            this.savedInstanceState = savedInstanceState;
        } else {
            this.lifecycleUtil = new LifecycleUtil(this.baseVideoView);
            this.lifecycleUtil.onCreate(savedInstanceState, this);
            this.eventLogger = new EventLogger(this.baseVideoView.getEventEmitter(), true, this.getClass().getSimpleName());
        }

    }

    private void initializeLifecycleUtil(View view) {
        if(this.baseVideoView == null) {
            this.findBaseVideoView(view);
            if(this.baseVideoView == null) {
                throw new IllegalStateException("A BaseVideoView must be wired up to the layout.");
            }

            this.lifecycleUtil = new LifecycleUtil(this.baseVideoView);
            this.lifecycleUtil.onCreate(this.savedInstanceState, this);
            this.eventLogger = new EventLogger(this.baseVideoView.getEventEmitter(), true, this.getClass().getSimpleName());
        }

        this.savedInstanceState = null;
    }

    private void findBaseVideoView(View view) {
        Log.v(TAG, "findBaseVideoView: view = " + view.getClass().getName());
        if(view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup)view;
            int childCount = viewGroup.getChildCount();

            for(int i = 0; i < childCount; ++i) {
                View child = viewGroup.getChildAt(i);
                if(child instanceof BaseVideoView) {
                    this.baseVideoView = (BaseVideoView)child;
                    break;
                }

                this.findBaseVideoView(child);
            }
        }

    }

    public void setContentView(View view) {
        super.setContentView(view);
        this.initializeLifecycleUtil(view);
    }

    public void setContentView(int layoutResId) {
        super.setContentView(layoutResId);
        View contentView = this.findViewById(16908290);
        this.initializeLifecycleUtil(contentView);
    }

    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        this.initializeLifecycleUtil(view);
    }

    protected void onStart() {
        Log.v(TAG, "onStart");
        super.onStart();
        this.lifecycleUtil.activityOnStart();
    }

    protected void onPause() {
        Log.v(TAG, "onPause");
        super.onPause();
        this.lifecycleUtil.activityOnPause();
    }

    protected void onResume() {
        Log.v(TAG, "onResume");
        super.onResume();
        this.lifecycleUtil.activityOnResume();
    }

    protected void onRestart() {
        Log.v(TAG, "onRestart");
        super.onRestart();
        this.lifecycleUtil.onRestart();
    }

    protected void onDestroy() {
        Log.v(TAG, "onDestroy");
        super.onDestroy();
        this.lifecycleUtil.activityOnDestroy();
    }

    protected void onStop() {
        Log.v(TAG, "onStop");
        super.onStop();
        this.lifecycleUtil.activityOnStop();
    }

    protected void onSaveInstanceState(final Bundle bundle) {
        this.baseVideoView.getEventEmitter().on("activitySaveInstanceState", new EventListener() {
            @Default
            public void processEvent(Event event) {
                BrightcovePlayerActivity.super.onSaveInstanceState(bundle);
            }
        });
        this.lifecycleUtil.activityOnSaveInstanceState(bundle);
    }
}
