package android.vb.com.vb.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class PurchaseHistory implements Parcelable {


    public static final Parcelable.Creator<PurchaseHistory> CREATOR = new Parcelable.Creator<PurchaseHistory>() {
        @Override
        public PurchaseHistory createFromParcel(Parcel source) {
            return new PurchaseHistory(source);
        }

        @Override
        public PurchaseHistory[] newArray(int size) {
            return new PurchaseHistory[size];
        }
    };
    /**
     * orderid : 5
     * orderno : 1209796134
     * userid : 134
     * seriesid : 3
     * amount : 150
     * paymentmade : 1
     * orderdate : 2017-09-13 00:00:00
     * orderstatus : 1
     * verify : 0
     * title : Twisted
     * description : Warning! Do not trust them!
     * longdescription : Amit Amritraaj, the owner of Amritraaj Constructions, has two years left to live. An empire of millions needs its next CEO. Amritraaj decides to play a game for the throne between his son, Arjun; his illegitimate son, Karan; and Veer, his employee who has been like a son to him<br>
     * generes : 16
     * cast : Nia Sharma, Namit Khanna, Tia Bajpai, Tanvi Vyas & Rahul Raj
     * epitype : paid
     * price : 150
     * folderid : 5564385782001
     * createdate : 2017-09-15
     * image : Twisted POSTER horizontal final (2).jpg
     * status : 1
     */

    private String orderid;
    private String orderno;
    private String userid;
    private String seriesid;
    private String amount;
    private String paymentmade;
    private String orderdate;
    private String orderstatus;
    private String verify;
    private String title;
    private String description;
    private String longdescription;
    private String generes;
    private String cast;
    private String epitype;
    private String price;
    private String folderid;
    private String createdate;
    private String image;
    private String status;

    public PurchaseHistory() {
    }

    protected PurchaseHistory(Parcel in) {
        this.orderid = in.readString();
        this.orderno = in.readString();
        this.userid = in.readString();
        this.seriesid = in.readString();
        this.amount = in.readString();
        this.paymentmade = in.readString();
        this.orderdate = in.readString();
        this.orderstatus = in.readString();
        this.verify = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.longdescription = in.readString();
        this.generes = in.readString();
        this.cast = in.readString();
        this.epitype = in.readString();
        this.price = in.readString();
        this.folderid = in.readString();
        this.createdate = in.readString();
        this.image = in.readString();
        this.status = in.readString();
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getSeriesid() {
        return seriesid;
    }

    public void setSeriesid(String seriesid) {
        this.seriesid = seriesid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentmade() {
        return paymentmade;
    }

    public void setPaymentmade(String paymentmade) {
        this.paymentmade = paymentmade;
    }

    public String getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(String orderdate) {
        this.orderdate = orderdate;
    }

    public String getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus;
    }

    public String getVerify() {
        return verify;
    }

    public void setVerify(String verify) {
        this.verify = verify;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLongdescription() {
        return longdescription;
    }

    public void setLongdescription(String longdescription) {
        this.longdescription = longdescription;
    }

    public String getGeneres() {
        return generes;
    }

    public void setGeneres(String generes) {
        this.generes = generes;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getEpitype() {
        return epitype;
    }

    public void setEpitype(String epitype) {
        this.epitype = epitype;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFolderid() {
        return folderid;
    }

    public void setFolderid(String folderid) {
        this.folderid = folderid;
    }

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PurchaseHistory{" +
                "orderid='" + orderid + '\'' +
                ", orderno='" + orderno + '\'' +
                ", userid='" + userid + '\'' +
                ", seriesid='" + seriesid + '\'' +
                ", amount='" + amount + '\'' +
                ", paymentmade='" + paymentmade + '\'' +
                ", orderdate='" + orderdate + '\'' +
                ", orderstatus='" + orderstatus + '\'' +
                ", verify='" + verify + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", longdescription='" + longdescription + '\'' +
                ", generes='" + generes + '\'' +
                ", cast='" + cast + '\'' +
                ", epitype='" + epitype + '\'' +
                ", price='" + price + '\'' +
                ", folderid='" + folderid + '\'' +
                ", createdate='" + createdate + '\'' +
                ", image='" + image + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.orderid);
        dest.writeString(this.orderno);
        dest.writeString(this.userid);
        dest.writeString(this.seriesid);
        dest.writeString(this.amount);
        dest.writeString(this.paymentmade);
        dest.writeString(this.orderdate);
        dest.writeString(this.orderstatus);
        dest.writeString(this.verify);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.longdescription);
        dest.writeString(this.generes);
        dest.writeString(this.cast);
        dest.writeString(this.epitype);
        dest.writeString(this.price);
        dest.writeString(this.folderid);
        dest.writeString(this.createdate);
        dest.writeString(this.image);
        dest.writeString(this.status);
    }
}