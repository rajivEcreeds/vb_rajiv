package android.vb.com.vb.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.vb.com.vb.R;
import android.vb.com.vb.activity.DetailedListActivity;
import android.vb.com.vb.activity.PlayerActivity;
import android.vb.com.vb.pojo.ContinueWatching;
import android.vb.com.vb.pojo.History;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by aniketrane on 29/09/17.
 */

public class ContinueWatchingAdapter extends RecyclerView.Adapter<ContinueWatchingAdapter.CustomViewHolder> {

    LayoutInflater layoutInflater;
    private List<History> watchingList = Collections.emptyList();
    Context mContext;

    public ContinueWatchingAdapter(Context context, List<History> watchingList){
        mContext = context;
        layoutInflater = LayoutInflater.from(context);
        this.watchingList = watchingList;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.continue_watching_row, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        final History history = watchingList.get(position);
        holder.textView.setText(history.getTitle());
        Picasso.with(mContext).load(history.getThumbnail()).into(holder.thumbnail);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, PlayerActivity.class)
                        .putExtra("isSingleVideo",true)
                        .putExtra("videoId", history.getVideoId())
                        .putExtra("timestamp",history.getMillisecond()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return watchingList.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        ImageView thumbnail;
        TextView textView;

        public CustomViewHolder(View itemView) {
            super(itemView);
            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
            textView = (TextView) itemView.findViewById(R.id.continue_watching_title);
        }
    }

}
