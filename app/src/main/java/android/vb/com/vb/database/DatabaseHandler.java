package android.vb.com.vb.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.vb.com.vb.pojo.History;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "vb";
    private static final String TABLE_HISTORY = "history";

    private static final String KEY_VIDEO_ID = "videoId";
    private static final String KEY_MILLISECOND = "millisecond";

    private static final int KEY_VIDEO_ID_INDEX = 0;
    private static final int KEY_MILLISECOND_INDEX = 1;
    private static final int KEY_TITLE_INDEX = 2;
    private static final int KEY_THUMBNAIL_INDEX = 3;

    private static final String KEY_TITLE = "title";
    private static final String KEY_THUMBNAIL = "thumbnail";
    private static final int KEY_SELECTED_LANGUAGE_INDEX = 1;
    private String TABLE_LANG = "language";
    private String KEY_SERIES_ID = "seriesId";
    private String KEY_SELECTED_LANGUAGE = "selectedLanguage";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String CREATE_TABLE = "CREATE TABLE " + TABLE_HISTORY + "("
                + KEY_VIDEO_ID + " TEXT PRIMARY KEY,"
                + KEY_MILLISECOND + " TEXT,"
                + KEY_TITLE + " TEXT,"
                + KEY_THUMBNAIL + " TEXT"
                + ")";

        String CREATE_TABLE_LANG = "CREATE TABLE " + TABLE_LANG + "("
                + KEY_SERIES_ID + " TEXT PRIMARY KEY,"
                + KEY_SELECTED_LANGUAGE + " TEXT"
                + ")";

        sqLiteDatabase.execSQL(CREATE_TABLE);
        sqLiteDatabase.execSQL(CREATE_TABLE_LANG);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_LANG);
        onCreate(sqLiteDatabase);
    }

    public void addHistory(History history) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_VIDEO_ID, history.getVideoId());
        values.put(KEY_MILLISECOND, history.getMillisecond());
        values.put(KEY_TITLE, history.getTitle());
        values.put(KEY_THUMBNAIL, history.getThumbnail());
        db.insertWithOnConflict(TABLE_HISTORY, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();

    }

    public void setLanguage(String seriesId, String selectedLanguage) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_SERIES_ID, seriesId);
        values.put(KEY_SELECTED_LANGUAGE, selectedLanguage);
        db.insertWithOnConflict(TABLE_LANG, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public String getLanguage(String seriesId) {
        try {
            String selectQuery = "SELECT * FROM " + TABLE_LANG + " WHERE " + KEY_SERIES_ID + "=" + seriesId;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                return cursor.getString(KEY_SELECTED_LANGUAGE_INDEX);
            }
            cursor.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public History getHistory(String videoId) {
        History history = new History();
        String selectQuery = "SELECT * FROM " + TABLE_HISTORY + " WHERE " + KEY_VIDEO_ID + "=" + videoId;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            history.setVideoId(cursor.getString(KEY_VIDEO_ID_INDEX));
            history.setMillisecond(cursor.getString(KEY_MILLISECOND_INDEX));
            history.setTitle(cursor.getString(KEY_TITLE_INDEX));
            history.setThumbnail(cursor.getString(KEY_THUMBNAIL_INDEX));
            return history;
        }
        cursor.close();
        db.close();
        return null;
    }

    public List<History> getAllHistory() {
        List<History> histories = new ArrayList<History>();
        String selectQuery = "SELECT  * FROM " + TABLE_HISTORY;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {

            if (cursor.moveToFirst()) {
                do {
                    History history = new History();
                    history.setTitle(cursor.getString(KEY_TITLE_INDEX));
                    history.setThumbnail(cursor.getString(KEY_THUMBNAIL_INDEX));
                    history.setMillisecond(cursor.getString(KEY_MILLISECOND_INDEX));
                    history.setVideoId(cursor.getString(KEY_VIDEO_ID_INDEX));
                    histories.add(history);
                } while (cursor.moveToNext());
            }

        }

        return histories;
    }
}
