package android.vb.com.vb.pojo;


import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {


    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
    /**
     * id : 126
     * ip_address : FE80::A00:27FF:FEFD:7261
     * name : test
     * lastname :
     * password : 32e1b88d83c23828f861781658b708c0
     * email : selvakmr160@gmail.com
     * mobile : 123456789
     * created_on : 0
     * last_login : 0000-00-00 00:00:00
     * lastupdate : 0000-00-00 00:00:00
     * active : 0
     */

    private String id;
    private String ip_address;
    private String name;
    private String lastname;
    private String password;
    private String email;
    private String mobile;
    private String created_on;
    private String last_login;
    private String lastupdate;
    private String active;

    public User() {
    }

    protected User(Parcel in) {
        this.id = in.readString();
        this.ip_address = in.readString();
        this.name = in.readString();
        this.lastname = in.readString();
        this.password = in.readString();
        this.email = in.readString();
        this.mobile = in.readString();
        this.created_on = in.readString();
        this.last_login = in.readString();
        this.lastupdate = in.readString();
        this.active = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(String lastupdate) {
        this.lastupdate = lastupdate;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", ip_address='" + ip_address + '\'' +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                ", created_on='" + created_on + '\'' +
                ", last_login='" + last_login + '\'' +
                ", lastupdate='" + lastupdate + '\'' +
                ", active='" + active + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.ip_address);
        dest.writeString(this.name);
        dest.writeString(this.lastname);
        dest.writeString(this.password);
        dest.writeString(this.email);
        dest.writeString(this.mobile);
        dest.writeString(this.created_on);
        dest.writeString(this.last_login);
        dest.writeString(this.lastupdate);
        dest.writeString(this.active);
    }
}