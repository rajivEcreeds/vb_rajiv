package android.vb.com.vb.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.vb.com.vb.R;
import android.vb.com.vb.activity.DetailedListActivity;
import android.vb.com.vb.pojo.Webseries;
import android.vb.com.vb.util.Constants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by SUJAN on 22-Aug-17.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeViewHolder> {

    public boolean isBing;
    LayoutInflater layoutInflater;
    private Context mContext;

    private List<Webseries> webseriesList = Collections.emptyList();

    public HomeAdapter(Context context, List<Webseries> webseries, Boolean isBing) {
        this.mContext = context;
        layoutInflater = LayoutInflater.from(context);
        this.isBing = isBing;
        this.webseriesList = webseries;
    }


    @Override
    public HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.adapter_home, parent, false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeViewHolder holder, int position) {
        final Webseries finalWebseries = webseriesList.get(position);
        Picasso.with(mContext).load("http://healthpro.in/vb/public/webseries/" + finalWebseries.getSeriesid() + ".jpg").into(holder.seriesImageView);
        holder.seriesNameTextView.setText(finalWebseries.getTitle());
        holder.episodeDetailTextView.setText(Html.fromHtml(finalWebseries.getDescription()).toString());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mContext.startActivity(new Intent(mContext, DetailedListActivity.class).putExtra("webseries", finalWebseries));
            }
        });
        if (isBing) {
            String currency;

            SharedPreferences userSharedPreferences = mContext.getSharedPreferences(Constants.USER_PREF, mContext.MODE_PRIVATE);
            String USER_COUNTRY = userSharedPreferences.getString(Constants.USER_PREF_USER_COUNTRY_KEY, "US");

            switch (USER_COUNTRY) {

                case "IND":
                    currency = mContext.getResources().getString(R.string.rupee_symbol);
                    priceVisible(holder, finalWebseries.getPrice(), currency);
                    break;
                case "PAK":
                    currency = "PKR";
                    priceVisible(holder, finalWebseries.getPakinr(), currency);
                    break;
                case "BENG":
                    currency = mContext.getResources().getString(R.string.bengali_symbol);
                    priceVisible(holder, finalWebseries.getBanginr(), currency);
                    break;
                case "UK":
                    currency = mContext.getResources().getString(R.string.pound_symbol);
                    priceVisible(holder, finalWebseries.getPound(), currency);
                    Log.d("POUND ", "finalWebseries.getPound(): " + finalWebseries.getPound());
                    break;
                case "US":
                    currency = mContext.getResources().getString(R.string.dollar_symbol);
                    priceVisible(holder, finalWebseries.getDollar(), currency);
                    break;
            }
        }

    }

    @Override
    public int getItemCount() {
        return webseriesList.size();
    }

    public void priceVisible(HomeAdapter.HomeViewHolder homeViewHolder, String price, String currency) {
        homeViewHolder.view.findViewById(R.id.adapter_home_price_frameLayout).setVisibility(View.VISIBLE);
        homeViewHolder.priceTextView.setText(currency + " " + price);
    }


    class HomeViewHolder extends RecyclerView.ViewHolder {

        View view;
        ImageView seriesImageView;
        TextView seriesNameTextView, newIndicatorTextView, episodeDetailTextView, priceTextView;

        public HomeViewHolder(View itemView) {
            super(itemView);

            view = itemView;
            seriesImageView = (ImageView) itemView.findViewById(R.id.adapter_home_serialImageView);
            seriesNameTextView = (TextView) itemView.findViewById(R.id.adapter_home_serialNameTextView);
            newIndicatorTextView = (TextView) itemView.findViewById(R.id.adapter_home_newIndicatorTextView);
            episodeDetailTextView = (TextView) itemView.findViewById(R.id.adapter_home_episodeDetailTextView);
            priceTextView = (TextView) itemView.findViewById(R.id.adapter_home_price_textView);

        }
    }
}
