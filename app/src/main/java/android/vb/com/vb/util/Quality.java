package android.vb.com.vb.util;

/**
 * Created by SUJAN on 10-Oct-17.
 */

public enum Quality {
    LOW(270),
    MEDIUM(360),
    HIGH(540);


    private final int qualityHeight;

    private Quality(int qualityHeight) {
        this.qualityHeight = qualityHeight;
    }

    /*public int toInt() {
        return this.qualityHeight;
    }*/

    public String toString() {
        return String.valueOf(this.qualityHeight);
    }

}
