package android.vb.com.vb.pojo;

/**
 * Created by SUJAN on 05-Sep-17.
 */

public class Season {


    /**
     * seasonid : 3
     * seriesid : 4
     * title : Season1
     * description : Maaya First Season
     * status : 1
     */

    private String seasonid;
    private String seriesid;
    private String title;
    private String description;
    private String status;

    public String getSeasonid() {
        return seasonid;
    }

    public void setSeasonid(String seasonid) {
        this.seasonid = seasonid;
    }

    public String getSeriesid() {
        return seriesid;
    }

    public void setSeriesid(String seriesid) {
        this.seriesid = seriesid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
