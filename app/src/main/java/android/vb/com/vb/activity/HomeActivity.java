package android.vb.com.vb.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AlignmentSpan;
import android.util.Log;
import android.vb.com.vb.R;
import android.vb.com.vb.adapter.ContinueWatchingAdapter;
import android.vb.com.vb.adapter.HomeAdapter;
import android.vb.com.vb.database.DatabaseHandler;
import android.vb.com.vb.pojo.Banner;
import android.vb.com.vb.pojo.History;
import android.vb.com.vb.pojo.User;
import android.vb.com.vb.pojo.Webseries;
import android.vb.com.vb.singleton.VolleySingleton;
import android.vb.com.vb.util.Constants;
import android.vb.com.vb.util.Defaults;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.mvc.imagepicker.ImagePicker;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.events.OnBannerClickListener;
import ss.com.bannerslider.views.BannerSlider;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HomeActivity extends AppCompatActivity {

    private static ViewPager bannerViewPager;
    private static int currentBanner = 0;
    List<ss.com.bannerslider.banners.Banner> remoteBanners = new ArrayList<>();
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private RecyclerView bingNowRecyclerView, forFreeRecyclerView, bannerRecyclerView;
    private List<Webseries> freeSeries = new ArrayList<>();
    private List<Webseries> paidSeries = new ArrayList<>();
    private Banner[] banners;
    private User[] user;
    private de.hdodenhof.circleimageview.CircleImageView nav_header_ImageView;
    private boolean isSkipped;
    private TextView nav_header_TextView, bingNowMoreTextView, forFreeMoreTextView;
    private Timer swipeTimer = new Timer();
    private boolean calledMyMethod = false;
    private BannerSlider bannerSlider;
    private List<History> watchingList = new ArrayList<>();
    private LinearLayout continueWatchingSection;
    private DatabaseHandler db;
    private Tracker mTracker;
    private Bus bus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bus = ((Defaults) getApplicationContext()).getBus();
        bus.register(this);

        /* Google Analytics */
        Defaults application = (Defaults) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        /* Google Analytics */

        db = new DatabaseHandler(HomeActivity.this);
        setContentView(R.layout.activity_home);
        init();
        setupToolbarMenu();
        setupNavigationView();
        initIsSkipped();
        initNavItemSelected();
        if (!isSkipped) {
            initUserData();
            initProfileImageClicked();
        } else {
            initOnClickLanding();
        }
        initWebseries();
        initBanner();

        if (Constants.getUserId() != null) {
            getHalfPlayedVideos();
        } else {
            getHalfPlayedVideosFromDB();
        }

        ImagePicker.setMinQuality(600, 600);


    }

    @Subscribe
    public void getMessage(String event) {
        if (event == Constants.CONTINUE_WATCHING_REFRESH) {
            continueWatchingSection.setVisibility(View.GONE);
            if (Constants.getUserId() != null) {
                getHalfPlayedVideos();
            } else {
                getHalfPlayedVideosFromDB();
            }
        }
    }

    private void init() {
        bannerSlider = (BannerSlider) findViewById(R.id.banner_slider1);
        bingNowMoreTextView = (TextView) findViewById(R.id.activity_home_bingnow_more_textView);
        forFreeMoreTextView = (TextView) findViewById(R.id.activity_home_forfree_more_textView);
        continueWatchingSection = (LinearLayout) findViewById(R.id.continue_watching_section);
    }

    private void setupNavigationView() {


        navigationView = (NavigationView) findViewById(R.id.activity_home_navigationView);

        //NavViewFont
        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            applyFontToMenuItem(mi);
        }


        View nav_header = navigationView.getHeaderView(0);
        drawerLayout = (DrawerLayout) findViewById(R.id.activity_home_drawerLayout);

        nav_header_ImageView = (de.hdodenhof.circleimageview.CircleImageView) nav_header.findViewById(R.id.nav_header_imageView);
        nav_header_TextView = (TextView) nav_header.findViewById(R.id.nav_header_textView);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    private void initIsSkipped() {
        SharedPreferences userSharedPreferences = getSharedPreferences(Constants.USER_PREF, MODE_PRIVATE);
        isSkipped = userSharedPreferences.getBoolean(Constants.USER_PREF_SKIP_KEY, false);

        if (isSkipped) {

            navigationView.getMenu().findItem(R.id.item_sign_out).setVisible(false);
            navigationView.getMenu().findItem(R.id.item_history).setVisible(false);
            navigationView.getMenu().findItem(R.id.item_log_in).setVisible(true);
            navigationView.getMenu().findItem(R.id.item_sign_in).setVisible(true);

        }

    }

    private void initNavItemSelected() {


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.item_home:
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.item_web_originals:
                        startActivity(new Intent(HomeActivity.this, WebOriginalsActivity.class));
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.item_trailers:
                        startActivity(new Intent(HomeActivity.this, TrailersActivity.class));
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.item_songs:
                        startActivity(new Intent(HomeActivity.this, SongsActivity.class));
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.item_history:
                        startActivity(new Intent(HomeActivity.this, PurchaseHistoryActivity.class));
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.item_about:
                        startActivity(new Intent(HomeActivity.this, AboutActivity.class));
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.item_contact_us:
                        startActivity(new Intent(HomeActivity.this, ContactUsActivity.class));
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.item_sign_out:

                        SharedPreferences.Editor userPreferenceEditor = getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE).edit();
                        userPreferenceEditor.clear();
                        userPreferenceEditor.apply();
                        userPreferenceEditor.commit();
                        startActivity(new Intent(HomeActivity.this, LandingActivity.class));
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.item_log_in:
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        break;

                    case R.id.item_sign_in:
                        startActivity(new Intent(HomeActivity.this, SignupActivity.class));
                        break;
                }
                return true;
            }
        });

    }

    private void setupToolbarMenu() {

        toolbar = (Toolbar) findViewById(R.id.activity_home_toolBar);
        toolbar.setTitle("Navigation View");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void initProfileImageClicked() {
        nav_header_ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPickImage(view);
            }
        });
    }

    private void initOnClickLanding() {
        nav_header_ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, LandingActivity.class));
            }
        });
    }

    public void onPickImage(View view) {
        ImagePicker.pickImage(this, "Select your image:");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String bitmapString;
        Bitmap bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
        if (bitmap != null) {
            nav_header_ImageView.setImageBitmap(bitmap);
            nav_header_ImageView.setBackgroundResource(0);

            bitmapString = Constants.bitmapToBase64(bitmap);
            setProfilePhoto(bitmapString);
            Log.d("GMAP", bitmapString);
        }
        InputStream is = ImagePicker.getInputStreamFromResult(this, requestCode, resultCode, data);
        if (is != null) {
            //If success
            //Toast.makeText(this, "Got input stream!", Toast.LENGTH_SHORT).show();
            try {
                is.close();
            } catch (IOException ex) {
                // ignore
            }
        } else {
            //If failed
            //Toast.makeText(this, "Failed to get input stream!", Toast.LENGTH_SHORT).show();
        }
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            Toast.makeText(this, String.valueOf(data.getBooleanExtra("needsRefresh", false)), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    private void initUserData() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "users",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("UTAG", response);
                        user = new Gson().fromJson(String.valueOf(response), User[].class);
                        if (user != null) {
                            nav_header_TextView.setText(user[0].getName().toUpperCase());
                        }
                        Picasso.with(HomeActivity.this).load("http://healthpro.in/vb/profile/" + user[0].getId() + ".jpg").into(nav_header_ImageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                nav_header_ImageView.setBackgroundResource(0);
                            }

                            @Override
                            public void onError() {
                            }
                        });

                        SharedPreferences.Editor userPreferenceEditor = getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE).edit();
                        userPreferenceEditor.putString(Constants.USER_PREF_NAME_KEY, user[0].getName());
                        userPreferenceEditor.putString(Constants.USER_PREF_LAST_NAME_KEY, user[0].getLastname());
                        userPreferenceEditor.putString(Constants.USER_PREF_MOBILE_KEY, user[0].getMobile());
                        userPreferenceEditor.apply();
                        userPreferenceEditor.commit();

                        Log.d("GRES", user[0].toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("GRES", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("userid", Constants.getUserId());    // TODO: 23-Sep-17

                return params;
            }
        };
        VolleySingleton.getInstance(HomeActivity.this).addToRequestQueue(stringRequest);


    }

    private void initWebseries() {


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constants.BASE_USER_URL + "webseries", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    JSONArray freeArray = response.getJSONArray("Free");
                    JSONArray paidArray = response.getJSONArray("Paid");

                    for (int i = 0; i < freeArray.length(); i++) {
                        Webseries webseries = new Webseries();
                        JSONObject jsonObject = freeArray.getJSONObject(i);

                        webseries.setSeriesid(jsonObject.getString("seriesid"));
                        webseries.setTitle(jsonObject.getString("title"));
                        webseries.setDescription(jsonObject.getString("description"));
                        webseries.setLongdescription(jsonObject.getString("longdescription"));
                        webseries.setGeneres(jsonObject.getString("generes"));
                        webseries.setCast(jsonObject.getString("cast"));
                        webseries.setEpitype(jsonObject.getString("epitype"));
                        webseries.setPrice(jsonObject.getString("price"));
                        webseries.setFolderid(jsonObject.getString("folderid"));
                        webseries.setTamilfolderid(jsonObject.getString("tamilfolderid"));
                        webseries.setTelugufolderid(jsonObject.getString("telugufolderid"));
                        webseries.setCreatedate(jsonObject.getString("createdate"));
                        webseries.setImage(jsonObject.getString("image"));
                        webseries.setStatus(jsonObject.getString("status"));

                        webseries.setPrice(jsonObject.getString("price"));
                        webseries.setPakinr(jsonObject.getString("pakinr"));
                        webseries.setPound(jsonObject.getString("pound"));
                        webseries.setBanginr(jsonObject.getString("banginr"));
                        webseries.setDollar(jsonObject.getString("dollar"));

                        freeSeries.add(webseries);
                    }

                    for (int i = 0; i < paidArray.length(); i++) {
                        Webseries webseries = new Webseries();
                        JSONObject jsonObject = paidArray.getJSONObject(i);

                        webseries.setSeriesid(jsonObject.getString("seriesid"));
                        webseries.setTitle(jsonObject.getString("title"));
                        webseries.setDescription(jsonObject.getString("description"));
                        webseries.setLongdescription(jsonObject.getString("longdescription"));
                        webseries.setGeneres(jsonObject.getString("generes"));
                        webseries.setCast(jsonObject.getString("cast"));
                        webseries.setEpitype(jsonObject.getString("epitype"));
                        webseries.setPrice(jsonObject.getString("price"));
                        webseries.setFolderid(jsonObject.getString("folderid"));
                        webseries.setTamilfolderid(jsonObject.getString("tamilfolderid"));
                        webseries.setTelugufolderid(jsonObject.getString("telugufolderid"));
                        webseries.setCreatedate(jsonObject.getString("createdate"));
                        webseries.setImage(jsonObject.getString("image"));
                        webseries.setStatus(jsonObject.getString("status"));



                        webseries.setPrice(jsonObject.getString("price"));
                        webseries.setPakinr(jsonObject.getString("pakinr"));
                        webseries.setPound(jsonObject.getString("pound"));
                        webseries.setBanginr(jsonObject.getString("banginr"));
                        webseries.setDollar(jsonObject.getString("dollar"));


                        if(jsonObject.getString("hinditrailer") != null){

                            webseries.setHinditrailer(jsonObject.getString("hinditrailer"));
                        }

                        if(jsonObject.getString("tamiltrailer") != null){

                            webseries.setTamiltrailer(jsonObject.getString("tamiltrailer"));

                        }

                        if(jsonObject.getString("telugutrailer") != null){

                            webseries.setTelugutrailer(jsonObject.getString("telugutrailer"));

                        }

                        paidSeries.add(webseries);
                    }


                    if (freeSeries != null) {
                        setFreeRecyclerView();
                        if (freeSeries.size() > 5) {
                            forFreeMoreTextView.setVisibility(View.VISIBLE);
                        }
                    }

                    if (paidSeries != null) {
                        setPaidRecyclerView();
                        if (paidSeries.size() > 5) {
                            bingNowMoreTextView.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();

            }
        };

        VolleySingleton.getInstance(HomeActivity.this).addToRequestQueue(jsonObjectRequest);

    }

    private void initBanner() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "slider",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("GLOG", "SLIDER :  " + response);
                            banners = new Gson().fromJson(response, Banner[].class);

                            if (banners != null) {
                                setBannerViewPager();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(HomeActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("device", "android");
                return params;
            }
        };
        VolleySingleton.getInstance(HomeActivity.this).addToRequestQueue(stringRequest);
    }


    private void setPaidRecyclerView() {

        HomeAdapter bingAdapter = new HomeAdapter(HomeActivity.this, paidSeries, true);

        bingNowRecyclerView = (RecyclerView) findViewById(R.id.activity_home_bingnow_recyclerView);
        bingNowRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        bingNowRecyclerView.setAdapter(bingAdapter);
        bingAdapter.notifyDataSetChanged();

    }

    private void setFreeRecyclerView() {

        HomeAdapter freeAdapter = new HomeAdapter(HomeActivity.this, freeSeries, false);


        forFreeRecyclerView = (RecyclerView) findViewById(R.id.activity_home_forfree_recyclerView);
        forFreeRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        forFreeRecyclerView.setAdapter(freeAdapter);
        freeAdapter.notifyDataSetChanged();

    }

    private void setBannerViewPager() {

        for (Banner banner : banners) {
            remoteBanners.add(new RemoteBanner("http://healthpro.in/vb/public/sliders/" + banner.getSlidername()));
        }
        bannerSlider.setBanners(remoteBanners);
        bannerSlider.setOnBannerClickListener(new OnBannerClickListener() {
            @Override
            public void onClick(int position) {
                startActivity(new Intent(HomeActivity.this, DetailedListActivity.class).putExtra("banners", banners[position]));
            }
        });
    }

    @Override
    public void onBackPressed() {

        /*AlertDialog alertDialog = new AlertDialog.Builder(HomeActivity.this).setTitle("VB").setMessage("Do you want to exit ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        ActivityCompat.finishAffinity(HomeActivity.this);
                    }
                })
                .setNegativeButton("No", null)
                .create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();
        */

        finish();
        ActivityCompat.finishAffinity(HomeActivity.this);
    }

    private void setProfilePhoto(final String bitmapString) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "saveimage",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(HomeActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("profilepicture", String.valueOf(bitmapString));
                params.put("user", Constants.getUserId());
                return params;
            }
        };
        VolleySingleton.getInstance(HomeActivity.this).addToRequestQueue(stringRequest);
    }


    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), 0, 0, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mi.setTitle(mNewTitle);
    }


    void getHalfPlayedVideos() {

        String url = "http://healthpro.in/vb/restserver/index.php/welcome/videotimesall";

        Map<String, String> params = new HashMap<>();
        params.put("userid", Constants.getUserId());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.has("message") && response.getJSONArray("message") != null) {

                        JSONArray jsonArray = response.getJSONArray("message");

                        watchingList.clear();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            History history = new History();
                            history.setVideoId(jsonArray.getJSONObject(i).getString("video"));
                            history.setTitle(jsonArray.getJSONObject(i).getString("title"));
                            history.setThumbnail(jsonArray.getJSONObject(i).getString("image"));
                            history.setMillisecond(jsonArray.getJSONObject(i).getString("leng"));
                            watchingList.add(history);
                        }

                        setUpContinuePlaying();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        VolleySingleton.getInstance(HomeActivity.this).addToRequestQueue(request);
    }

    private void getHalfPlayedVideosFromDB() {
        List<History> histories = db.getAllHistory();
        if (!histories.isEmpty()) {
            this.watchingList = histories;
        }
        setUpContinuePlaying();
    }

    private void setUpContinuePlaying() {
        RecyclerView continueWatching = (RecyclerView) findViewById(R.id.continue_playing_recyclerView);
        continueWatching.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        ContinueWatchingAdapter watchingAdapter = new ContinueWatchingAdapter(HomeActivity.this, watchingList);
        continueWatching.setAdapter(watchingAdapter);
        watchingAdapter.notifyDataSetChanged();
        continueWatchingSection.setVisibility(View.VISIBLE);

    }
}

