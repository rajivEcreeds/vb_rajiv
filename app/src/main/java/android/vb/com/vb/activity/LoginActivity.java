package android.vb.com.vb.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.vb.com.vb.R;
import android.vb.com.vb.singleton.VolleySingleton;
import android.vb.com.vb.util.Constants;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {


    private static final int GOOGLE_REQ_CODE = 292;
    private CallbackManager fbCallbackManager;
    private Button loginButton;
    private EditText emailEditText, passwordEditText;
    private ImageView googleSignImageView, facebookLoginImageView;
    private TextView forgotPasswordTextView;
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        Constants.fbKeyHash(this);
        init();
        onClick();





        googleSignInClicked();
        fbLoginClicked();



    }

    private void init() {
        loginButton = (Button) findViewById(R.id.activity_login_loginButton);
        emailEditText = (EditText) findViewById(R.id.activity_login_email_editText);
        passwordEditText = (EditText) findViewById(R.id.activity_login_password_editText);
        forgotPasswordTextView = (TextView) findViewById(R.id.activity_login_forgot_password_textView);

        facebookLoginImageView = (ImageView) findViewById(R.id.login_button);
        fbCallbackManager = CallbackManager.Factory.create();

        googleSignImageView = (ImageView) findViewById(R.id.google_button);
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();


    }

    private void onClick() {
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initLogin();
            }
        });
        forgotPasswordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });

    }

    private void initLogin() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "login",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonRootObject = new JSONObject(response);
                            String message = jsonRootObject.optString("message");
                            String userid = jsonRootObject.optString("id");


                            if (message.equals("Success")) {


                                Constants.setUserId(userid);     // TODO: 23-Sep-17


                                SharedPreferences.Editor userPreferenceEditor = getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE).edit();
                                userPreferenceEditor.putBoolean(Constants.USER_PREF_LOGIN_KEY, true);
                                userPreferenceEditor.putString(Constants.USER_PREF_EMAIL_KEY, emailEditText.getText().toString());     // TODO: 23-Sep-17

                                userPreferenceEditor.putString(Constants.USER_PREF_PASSWORD_KEY, passwordEditText.getText().toString());     // TODO: 23-Sep-17

                                userPreferenceEditor.putBoolean(Constants.USER_PREF_SKIP_KEY, false);
                                userPreferenceEditor.apply();
                                userPreferenceEditor.commit();


                                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                            } else if (message.equals("Username or Password Wrong")) {

                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(LoginActivity.this, "Incorrect username or password", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String time = android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss a", new java.util.Date()).toString();
                Map<String, String> params = new HashMap<>();
                params.put("email", emailEditText.getText().toString());
                params.put("password", Constants.md5(passwordEditText.getText().toString()));
                params.put("logintime", time.substring(0, time.length() - 2));
                params.put("ip_address", Constants.getIPAddress(false));
                return params;
            }
        };
        VolleySingleton.getInstance(LoginActivity.this).addToRequestQueue(stringRequest);
    }

    private void initFb() {

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(fbCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("GFB", "onSuccess: " + loginResult.getAccessToken());
                String userEmailId = loginResult.getAccessToken().getUserId();

                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        displayUserFbInfo(object);
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name,last_name,email,id,cover,picture.type(large)");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {

                Log.d("GFB", "onCancel: ");

            }

            @Override
            public void onError(FacebookException error) {
                Log.d("GFB", "onError" + error.toString());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fbCallbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handlerResult(result);
        }
    }


    private void displayUserFbInfo(JSONObject jsonObject) {

        String first_name = "";
        String last_name = "";
        String email = "";
        String id = "";
        String profilePicUrl = "";

        try {
            first_name = jsonObject.getString("first_name");
            last_name = jsonObject.getString("last_name");
            email = jsonObject.getString("email");
            id = jsonObject.getString("id");

            profilePicUrl = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");

            initSocialLogin(first_name, last_name, email);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("GFBB", "fName: " + first_name
                + "\nlName: " + last_name
                + "\nemail: " + email
                + "\npicture: " + profilePicUrl
                + "\nid: " + id);
    }

    private void googleSignInClicked() {
        googleSignImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent, GOOGLE_REQ_CODE);
            }
        });
    }

    private void fbLoginClicked() {
        facebookLoginImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();
                    initFb();
                } else {
                    initFb();
                }


            }
        });
    }

    private void handlerResult(GoogleSignInResult googleSignInResult) {

        if (googleSignInResult.isSuccess()) {
            GoogleSignInAccount googleSignInAccount = googleSignInResult.getSignInAccount();
            String[] name = googleSignInAccount.getDisplayName().split(" ");
            String fname = name[0];
            String lname = name[1];
            String email = googleSignInAccount.getEmail();
            //String img_url = googleSignInAccount.getPhotoUrl().toString();

            initSocialLogin(fname, lname, email);

            Log.d("GGLOG", "fName: " + fname + "\nLName: " + lname + "\nEmail: " + email + "\nPic: ");
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void initSocialLogin(final String fName, final String lName, final String email) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "sociallogin",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonRootObject = new JSONObject(response);
                            String userid = jsonRootObject.optString("id");


                            if (!response.contains("Some Details Missing")) {

                                Constants.setUserId(userid);     // TODO: 23-Sep-17

                                SharedPreferences.Editor userPreferenceEditor = getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE).edit();
                                userPreferenceEditor.putBoolean(Constants.USER_PREF_LOGIN_KEY, true);
                                userPreferenceEditor.putBoolean(Constants.USER_PREF_SOCIAL_LOGIN_KEY, true);
                                userPreferenceEditor.putString(Constants.USER_PREF_EMAIL_KEY, email);     // TODO: 23-Sep-17

                                //userPreferenceEditor.putString(Constants.USER_PREF_PASSWORD_KEY, Constants.encryptThis(LoginActivity.this, passwordEditText.getText().toString()));
                                userPreferenceEditor.putBoolean(Constants.USER_PREF_SKIP_KEY, false);
                                userPreferenceEditor.apply();
                                userPreferenceEditor.commit();


                                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                            }/* else if (message.equals("Username or Password Wrong")) {

                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(SignupActivity.this, "Incorrect username or password", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }*/

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String time = android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss a", new java.util.Date()).toString();
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("name", fName);
                params.put("lastname", lName);
                params.put("ip_address", Constants.getIPAddress(false));
                params.put("logintime", time.substring(0, time.length() - 2));
                return params;
            }
        };
        VolleySingleton.getInstance(LoginActivity.this).addToRequestQueue(stringRequest);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
