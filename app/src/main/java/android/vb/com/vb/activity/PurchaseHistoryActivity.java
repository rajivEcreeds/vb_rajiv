package android.vb.com.vb.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.vb.com.vb.R;
import android.vb.com.vb.adapter.PurchaseAdapter;
import android.vb.com.vb.pojo.PurchaseHistory;
import android.vb.com.vb.singleton.VolleySingleton;
import android.vb.com.vb.util.Constants;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PurchaseHistoryActivity extends AppCompatActivity {


    private Toolbar toolbar;
    private RecyclerView purchaseHistoryRecyclerView;
    //private List<PurchaseHistory> purchaseHistoriesList = new ArrayList<>();
    private PurchaseHistory[] purchaseHistoriesList;
    private LinearLayout noDataLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_history);

        initToolbar();
        init();
        initPurchaseHistory();
    }

    private void init() {

        noDataLinearLayout = (LinearLayout) findViewById(R.id.activity_purchase_history_nodata_LinearLayout);
    }


    private void initToolbar() {

        toolbar = (Toolbar) findViewById(R.id.activity_purchase_history_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Purchase History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

    }

    private void initPurchaseHistory() {

        final String userId = Constants.getUserId();     // TODO: 23-Sep-17
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "orderspaid",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (!response.contains("No result")) {
                            Log.d("GP", "onResponse: " + response);
                            purchaseHistoriesList = new Gson().fromJson(response, PurchaseHistory[].class);

                            if (purchaseHistoriesList != null) {

                                initRecyclerView();
                            }
                        } else {
                            noDataLinearLayout.setVisibility(View.VISIBLE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(PurchaseHistoryActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("userid", userId);
                return params;
            }
        };
        VolleySingleton.getInstance(PurchaseHistoryActivity.this).addToRequestQueue(stringRequest);

    }


    private void initRecyclerView() {

        PurchaseAdapter purchaseAdapter = new PurchaseAdapter(this, Arrays.asList(purchaseHistoriesList));
        purchaseHistoryRecyclerView = (RecyclerView) findViewById(R.id.activity_purchase_history_recyclerView);
        purchaseHistoryRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        purchaseHistoryRecyclerView.setAdapter(purchaseAdapter);
        purchaseAdapter.notifyDataSetChanged();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
