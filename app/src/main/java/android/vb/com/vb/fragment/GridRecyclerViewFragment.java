package android.vb.com.vb.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.vb.com.vb.R;
import android.vb.com.vb.adapter.GridAdapter;
import android.vb.com.vb.pojo.Webseries;
import android.vb.com.vb.singleton.VolleySingleton;
import android.vb.com.vb.util.Constants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class GridRecyclerViewFragment extends Fragment {


    private RecyclerView gridRecyclerView;
    private View view;
    private Webseries[] freeSeries;
    private Webseries[] paidSeries;
    private boolean isBing;

    public GridRecyclerViewFragment() {
        // Required empty public constructor
    }

    public GridRecyclerViewFragment(boolean isBing) {
        // Required empty public constructor

        this.isBing = isBing;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_grid_recycler_view, container, false);
        initWebseries();

        return view;


    }


    private void setGridRecyclerView(Webseries[] webseries, boolean isBingBoolean) {

        GridAdapter gridAdapter = new GridAdapter(getActivity(), Arrays.asList(webseries), isBingBoolean);
        gridRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_grid_recycler_view_recyclerview);
        gridRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false));
        gridRecyclerView.setAdapter(gridAdapter);
        gridAdapter.notifyDataSetChanged();

    }

    private void initWebseries() {


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constants.BASE_USER_URL + "webseries", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    JSONArray freeArray = response.getJSONArray("Free");
                    JSONArray paidArray = response.getJSONArray("Paid");

                    freeSeries = new Gson().fromJson(String.valueOf(freeArray), Webseries[].class);
                    paidSeries = new Gson().fromJson(String.valueOf(paidArray), Webseries[].class);


                    if (isBing) {


                        if (paidSeries != null) {
                            setGridRecyclerView(paidSeries, isBing);
                            /*if (paidSeries.length > 5) {
                                bingNowMoreTextView.setVisibility(View.VISIBLE);
                            }*/
                        }


                    } else {
                        if (freeSeries != null) {
                            setGridRecyclerView(freeSeries, isBing);
                            /*if (freeSeries.length > 5) {
                                forFreeMoreTextView.setVisibility(View.VISIBLE);
                            }*/
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();

            }
        };

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

    }

}
