package android.vb.com.vb.pojo;

/**
 * Created by aniketrane on 19/09/17.
 */

public class History {

    private String videoId;
    private String millisecond;
    private String title;
    private String thumbnail;

    public History() {
    }

    public History(String videoId, String millisecond, String title, String thumbnail) {
        this.videoId = videoId;
        this.millisecond = millisecond;
        this.thumbnail = thumbnail;
        this.title = title;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getMillisecond() {
        return millisecond;
    }

    public void setMillisecond(String millisecond) {
        this.millisecond = millisecond;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

}
