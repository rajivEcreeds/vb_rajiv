package android.vb.com.vb.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.vb.com.vb.R;
import android.vb.com.vb.pojo.Video;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by SUJAN on 28-Aug-17.
 */

public class VerticalEpisodeAdapter extends RecyclerView.Adapter<VerticalEpisodeAdapter.VerticalEpisodeViewHolder> {


    LayoutInflater layoutInflater;
    List<Video> videosList;
    private Context mContext;
    private int[] episodeImageArray = {R.drawable.rainpromo, R.drawable.gehraiyaan, R.drawable.mayaa, R.drawable.spotlight, R.drawable.twisted1};
    private String[] episodeNameArray = {"Rain", "Gehraiyaan", "Mayaa", "Spotlight", "Twisted"};
    private boolean isPaid;
    private OnClickListener onClickListener;

    public VerticalEpisodeAdapter(Context context, List<Video> videos, boolean isBinge) {
        this.mContext = context;
        layoutInflater = LayoutInflater.from(context);
        this.videosList = videos;
        this.isPaid = isBinge;
    }

    @Override
    public VerticalEpisodeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.adapter_vertical_episode, parent, false);

        return new VerticalEpisodeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VerticalEpisodeViewHolder holder, final int position) {

        final Video finalVideo = videosList.get(position);
        Picasso.with(mContext).load(finalVideo.getImages().getThumbnail().getSrc()).into(holder.episodeImageView);
        holder.episodeNumberTextView.setText("EP" + (1 + position + " "));//
        holder.episodeNameTextView.setText(finalVideo.getName());
        holder.episodeDetailTextView.setText((finalVideo.getDescription() == null) ? "No Description" : finalVideo.getDescription());
        holder.episodeTimeTextView.setText(String.format("%d:%d min",
                TimeUnit.MILLISECONDS.toMinutes(Long.parseLong(String.valueOf(finalVideo.getDuration()))),
                TimeUnit.MILLISECONDS.toSeconds(Long.parseLong(String.valueOf(finalVideo.getDuration()))) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(Long.parseLong(String.valueOf(finalVideo.getDuration()))))
        ));


        if (!isPaid) {

            holder.view.findViewById(R.id.adapter_vertical_episode_tableLayout).setAlpha(0.2f);
            holder.view.setClickable(false);

        } else {


            holder.view.findViewById(R.id.adapter_vertical_episode_tableLayout).setAlpha(1f);
            holder.view.setClickable(true);

            holder.view.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    onClickListener.onClick(position);
                }
            });


        }

        if (position % 2 == 0) {
            holder.view.setBackgroundColor(Color.parseColor("#0B0C10"));
        } else {
            holder.view.setBackgroundColor(Color.parseColor("#0C0F14"));
        }
    }

    @Override
    public int getItemCount() {
        return videosList.size();
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener {
        void onClick(int position);
    }

    class VerticalEpisodeViewHolder extends RecyclerView.ViewHolder {

        View view;
        ImageView episodeImageView;
        TextView episodeNumberTextView, episodeNameTextView, episodeTimeTextView, episodeDetailTextView;

        public VerticalEpisodeViewHolder(View itemView) {
            super(itemView);

            view = itemView;
            episodeImageView = (ImageView) itemView.findViewById(R.id.adapter_vertical_episode_episodeImageView);
            episodeNumberTextView = (TextView) itemView.findViewById(R.id.adapter_vertical_episode_episodeNumberTextView);
            episodeNameTextView = (TextView) itemView.findViewById(R.id.adapter_vertical_episode_episodeNameTextView);
            episodeTimeTextView = (TextView) itemView.findViewById(R.id.adapter_vertical_episode_episodeTimeTextView);
            episodeDetailTextView = (TextView) itemView.findViewById(R.id.adapter_vertical_episode_episodeDetailsTextView);

        }
    }
}
