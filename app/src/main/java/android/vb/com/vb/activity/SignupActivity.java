package android.vb.com.vb.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.vb.com.vb.R;
import android.vb.com.vb.singleton.VolleySingleton;
import android.vb.com.vb.util.Constants;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignupActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final int GOOGLE_REQ_CODE = 2912;
    private Button signupButton;
    private EditText nameEditText, lastNameEditText, emailEditText, passwordEditText, phoneEditText;
    private CallbackManager fbCallbackManager;
    private ImageView googleSignImageView, facebookLoginImageView;
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_signup);


        Constants.fbKeyHash(this);
        init();
        onClick();
        googleSignInClicked();
        fbLoginClicked();


    }

    private void init() {

        signupButton = (Button) findViewById(R.id.activity_signup_signupButton);
        nameEditText = (EditText) findViewById(R.id.activity_signup_name_editText);
        lastNameEditText = (EditText) findViewById(R.id.activity_signup_last_name_editText);
        emailEditText = (EditText) findViewById(R.id.activity_signup_email_editText);
        passwordEditText = (EditText) findViewById(R.id.activity_signup_password_editText);
        phoneEditText = (EditText) findViewById(R.id.activity_signup_phone_editText);


        //

        facebookLoginImageView= (ImageView) findViewById(R.id.activity_signup_login_button);
        fbCallbackManager = CallbackManager.Factory.create();

        googleSignImageView= (ImageView) findViewById(R.id.activity_signup_google_button);
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();
    }

    private void onClick() {
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initSingup();
            }
        });
    }

    private void initSingup() {


        if (Constants.isEmailValid(emailEditText.getText().toString())) {

            if (Constants.isPasswordValid(passwordEditText.getText().toString())) {

                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "create",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonRootObject = new JSONObject(response);
                                    String message = jsonRootObject.optString("message");
                                    String userid = jsonRootObject.optString("id");


                                    if (message.equals("Email Exists")) {
                                        Toast.makeText(SignupActivity.this, "Email already exists", Toast.LENGTH_SHORT).show();
                                    }

                                    if (message.equals("Mobile Exists")) {
                                        Toast.makeText(SignupActivity.this, "Mobile number already exists", Toast.LENGTH_SHORT).show();
                                    }


                                    if (message.equals("Success")) {

                                        Constants.setUserId(userid);     // TODO: 23-Sep-17

                                        SharedPreferences.Editor userPreferenceEditor = getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE).edit();
                                        userPreferenceEditor.putBoolean(Constants.USER_PREF_LOGIN_KEY, true);
                                        userPreferenceEditor.putString(Constants.USER_PREF_EMAIL_KEY, emailEditText.getText().toString());     // TODO: 23-Sep-17

                                        userPreferenceEditor.putString(Constants.USER_PREF_PASSWORD_KEY, passwordEditText.getText().toString());    // TODO: 23-Sep-17

                                        userPreferenceEditor.putBoolean(Constants.USER_PREF_SKIP_KEY, false);
                                        userPreferenceEditor.apply();
                                        userPreferenceEditor.commit();


                                        startActivity(new Intent(SignupActivity.this, HomeActivity.class));
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(SignupActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("name", nameEditText.getText().toString());
                        params.put("lastname", lastNameEditText.getText().toString());
                        params.put("mobile", phoneEditText.getText().toString());
                        params.put("email", emailEditText.getText().toString());
                        params.put("password", Constants.md5(passwordEditText.getText().toString()));
                        params.put("lastupdate", String.valueOf(Calendar.getInstance().getTime()));
                        params.put("ip_address", Constants.getIPAddress(false));
                        return params;
                    }

                };
                VolleySingleton.getInstance(SignupActivity.this).addToRequestQueue(stringRequest);

            } else {
                Toast.makeText(this, "Password should have minimum 8 characters and atlest 1 digit", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Invalid Email", Toast.LENGTH_SHORT).show();
        }
    }

    //

    private void googleSignInClicked() {
        googleSignImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent, GOOGLE_REQ_CODE);
            }
        });
    }

    private void fbLoginClicked() {
        facebookLoginImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();
                    initFb();
                } else {
                    initFb();
                }
            }
        });
    }


    private void initFb() {

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(fbCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("GFB", "onSuccess: " + loginResult.getAccessToken());
                String userEmailId = loginResult.getAccessToken().getUserId();

                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        displayUserFbInfo(object);
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name,last_name,email,id,cover,picture.type(large)");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d("GFB", "onCancel: ");

            }

            @Override
            public void onError(FacebookException error) {
                Log.d("GFB", "onError");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fbCallbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handlerResult(result);
        }
    }


    private void displayUserFbInfo(JSONObject jsonObject) {

        String first_name = "";
        String last_name = "";
        String email = "";
        String id = "";
        String profilePicUrl = "";

        try {
            first_name = jsonObject.getString("first_name");
            last_name = jsonObject.getString("last_name");
            email = jsonObject.getString("email");
            id = jsonObject.getString("id");

            profilePicUrl = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");

            //Log.d("FPIC", Picasso.with(this).load(profilePicUrl).toString());
            initSocialLogin(first_name, last_name, email);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("GFBB", "fName: " + first_name
                + "\nlName: " + last_name
                + "\nemail: " + email
                + "\npicture: " + profilePicUrl
                + "\nid: " + id);
    }

    private void handlerResult(GoogleSignInResult googleSignInResult) {

        if (googleSignInResult.isSuccess()) {
            GoogleSignInAccount googleSignInAccount = googleSignInResult.getSignInAccount();
            String[] name = googleSignInAccount.getDisplayName().split(" ");
            String fname = name[0];
            String lname = name[1];
            String email = googleSignInAccount.getEmail();
            //String img_url = googleSignInAccount.getPhotoUrl().toString();

            initSocialLogin(fname, lname, email);

            Log.d("GGLOG", "fName: " + fname + "LName: " + lname + "\nEmail: " + email + "\nPic: ");
        }
    }


    private void initSocialLogin(final String fName, final String lName, final String email) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_USER_URL + "sociallogin",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonRootObject = new JSONObject(response);
                            String message = jsonRootObject.optString("message");


                            if (!response.contains("Some Details Missing")) {

                                String userid = jsonRootObject.optString("id");
                                Constants.setUserId(userid);     // TODO: 23-Sep-17

                                SharedPreferences.Editor userPreferenceEditor = getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE).edit();
                                userPreferenceEditor.putBoolean(Constants.USER_PREF_LOGIN_KEY, true);
                                userPreferenceEditor.putBoolean(Constants.USER_PREF_SOCIAL_LOGIN_KEY, true);
                                userPreferenceEditor.putString(Constants.USER_PREF_EMAIL_KEY, email);     // TODO: 23-Sep-17

                                //userPreferenceEditor.putString(Constants.USER_PREF_PASSWORD_KEY, Constants.encryptThis(LoginActivity.this, passwordEditText.getText().toString()));
                                userPreferenceEditor.putBoolean(Constants.USER_PREF_SKIP_KEY, false);
                                userPreferenceEditor.apply();
                                userPreferenceEditor.commit();


                                startActivity(new Intent(SignupActivity.this, HomeActivity.class));
                            }/* else if (message.equals("Username or Password Wrong")) {

                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(SignupActivity.this, "Incorrect username or password", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }*/

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SignupActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String time = android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss a", new java.util.Date()).toString();
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("name", fName);
                params.put("lastname", lName);
                params.put("ip_address", Constants.getIPAddress(false));
                params.put("logintime", time.substring(0, time.length() - 2));
                //params.put("password", Constants.md5(passwordEditText.getText().toString()));
                return params;
            }
        };
        VolleySingleton.getInstance(SignupActivity.this).addToRequestQueue(stringRequest);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}




